WineDivine 1.0rc

Eine Anwendung zur Verwaltung von Weinbestellungen.

Features

Es können Kunden angelegt und zu diesen können Bestellungen erfaßt werden.

Sofern Kunden und Bestellungen erfaßt wurden können diese gespeichert werden.
Per Default werden die Daten per Serialisierung in eine Datei gespeichert.

Wird die Anwendung mit dem Argument db gestart werden die Daten in eine Datenbank gespeichert.



Konfiguration

Die Definition der DatenBank Konfiguration erfolgt in der Datei dbconnect.properties. Diese liegt im Package winedivine.database und wird zur Laufzeit ausgelesen.

Der dort definierte Name der Datenbank-Datei wird verwendet.

Default : 	./winedivine.db 



Ausblick

Vielleicht eine Umstellung auf JPA , hibernate und h2 ?

...
