package winedivine.ui;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.Random;

import javax.swing.ImageIcon;

public class IconFactory {
	public enum KundenImage {
		A("FACE25.jpg"), B("FACE27.jpg") ,C("FACE29.jpg"), D("FACE32.jpg"), E("FACE33.jpg"), 
		F("woman17.jpg"),G("woman30.jpg"), H("woman31.jpg"), I("woman71.jpg");
		
	    private String name;
	    private KundenImage(String name) {
	    	this.name= name;
	    }
	    public String getName() {
	        return name;
	    }

	}
	// get an array of all the cards
	private static KundenImage[]kundenimages=KundenImage.values();
	// this generates random numbers
	private static Random random = new Random();
	// choose a card at random
	public static String getDefaultKundenImage(){
	     return kundenimages[random.nextInt(kundenimages.length)].getName();
	}
	public static String USER_DIR =  System.getProperty("user.dir");
	public static String FILE_SEP = System.getProperty("file.separator");
	public static String IMAGE_PATH = "img/" ;
	public static String APP_ICON = "icons/" + "logo.png";
	
	
	public static Image getImage(String imageName) {
		if (imageName == null) 
			imageName = APP_ICON;
		return Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemClassLoader().getResource(imageName));
	}
	
	public static ImageIcon getScaledImageIcon(String filename, int width, int height) {
		return new ImageIcon ((new ImageIcon (getImage(IMAGE_PATH + filename))).getImage().getScaledInstance(width,height,  Image.SCALE_SMOOTH));
	}

	public static ImageIcon getImageIcon(String filename) {
		return getScaledImageIcon(filename, 32, 32);
	}

	public static String detRelPath(String filename){
		return USER_DIR + FILE_SEP + filename;
	}
	

}
