package winedivine.ui;

public interface IConstants {
	public final static String AC_kunde = "kunde";
	
	public final static String AC_kunde_angelegt = "kunde angelegt";

	public final static String AC_bestellung_neu = "bestellung neu";
	
	public final static String AC_neu = "neu";
	public static final String AC_einzelV = "einzelV"; 
	public static final String AC_grossV = "grossV"; 
	public static final String AC_gesellschaft = "gesellschaft"; 
	
	public final static String AC_bestellung = "bestellung";

	public final static String AC_sichern = "sichern";
	public final static String AC_oeffnen = "oeffnen";
	public final static String AC_beenden = "beenden";
	
	public final static String AC_aendern = "aendern";
	public final static String AC_loeschen = "loeschen";
	public final static String AC_autorin = "autorin";
	public final static String AC_hilfe = "hilfe";
	public final static String AC_info = "info";

}
