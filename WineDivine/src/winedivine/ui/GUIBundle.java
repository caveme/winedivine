package winedivine.ui;

import java.util.MissingResourceException;
import java.util.ResourceBundle;


/**
 * @Author Markus Schubert
 * 
 *         Datum 02.06.2017
 *
 *
 */
public class GUIBundle {

	/**
	 * Access for the ressourceBundle for multilanguage support
	 * 
	 * @author Markus Schubert
	 *
	 */
	private static class BundleHolder {
		private static final ResourceBundle bundle = ResourceBundle
				.getBundle("winedivine.ui.winedivine");
	}

	/**
	 * No need to create an instance
	 */
	private GUIBundle() {
	}

	/**
	 * Get the string defined in the property file.
	 *
	 * @param key
	 *            the key for the desired string
	 * @exception NullPointerException
	 *                if <code>key</code> is <code>null</code>
	 * @exception MissingResourceException
	 *                if no object for the given key can be found
	 * @exception ClassCastException
	 *                if the object found for the given key is not a string
	 * @return the string for the given key
	 */
	public static String getString(String key) {
		String result = "-N.A.-";
		try {
			result = BundleHolder.bundle.getString(key);
		} catch (Exception ex) {
			System.out.println(ex.getLocalizedMessage());
		}

		return result;
	}

}
