package winedivine.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import winedivine.controller.Controller;

public class WineDivineFrame extends JFrame {

	private static final String APP_TITLE = "WineDivine";

	private static final int framewidth = 1024;
	private static final int frameheight = 600;

	private static final int kundenheight = 300;
	private static final int kundentypwidth = 200;
	private static final int bestellungheight = 300;

	private Controller ctrl;
	private JPanel pnlMain = new JPanel();
	private PanelBestellung pnlBestellung = new PanelBestellung();
	private JPanel pnlKunden = new JPanel();
	private JLabel image = new JLabel();
	private Kundenliste pnlKundenListe = new Kundenliste();
	private RadioButtons pnlKundenTyp = new RadioButtons();
	private MenueWeinvertrieb menubar = new MenueWeinvertrieb();
	
	/**
	 * 
	 * @param ctrl
	 */
	public WineDivineFrame(Controller ctrl) {
		super(WineDivineFrame.APP_TITLE);
		this.ctrl = ctrl;
		ctrl.setMainframe(this);
		buildUI();
	}
	/**
	 * 
	 */
	private void buildUI() {

		pnlKunden.setLayout(new BorderLayout());
		pnlKunden.setMinimumSize(new Dimension(framewidth, kundenheight));
		pnlKunden.setPreferredSize(new Dimension(framewidth, kundenheight));

		pnlKundenListe.setBorder(BorderFactory.createEtchedBorder());
		pnlKundenTyp.setBorder(BorderFactory.createEtchedBorder());
		pnlKundenTyp.setMinimumSize(new Dimension(kundentypwidth, kundenheight));
		pnlKundenTyp.setPreferredSize(new Dimension(kundentypwidth, kundenheight));

		pnlBestellung.setBorder(BorderFactory.createEtchedBorder());
		pnlBestellung.setMinimumSize(new Dimension(framewidth, bestellungheight));
		pnlBestellung.setPreferredSize(new Dimension(framewidth, bestellungheight));

		image.setSize(64, 64);
		image.setFont(new Font("Courier New", Font.ITALIC, 12));
		image.setBorder(BorderFactory.createLoweredBevelBorder());
		image.setText("kundenimage");

		JPanel left = new JPanel();
		left.setLayout(new BorderLayout());
		left.add(pnlKundenListe, BorderLayout.CENTER);
		left.add(image, BorderLayout.EAST);

		pnlKunden.add(left, BorderLayout.CENTER);
		pnlKunden.add(pnlKundenTyp, BorderLayout.EAST);
		
		pnlMain.setLayout(new BorderLayout());
		pnlMain.add(pnlKunden, BorderLayout.CENTER);
		pnlMain.add(pnlBestellung, BorderLayout.SOUTH);

		addListeners();

		this.setJMenuBar((JMenuBar) menubar);
		this.getContentPane().add(pnlMain);

		this.setIconImage(IconFactory.getImageIcon(IconFactory.APP_ICON).getImage());

		this.setPreferredSize(new Dimension(framewidth, frameheight));
		this.setMinimumSize(new Dimension(framewidth, frameheight));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
//		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

	private void addListeners() {
		//Listenselection an Controller rückmelden
		pnlKundenListe.getjList().addListSelectionListener(getCtrl());
		pnlKundenListe.getjList().addMouseListener(getCtrl());
		//Menüaktionen an Controller rückmelden
		menubar.addListener(getCtrl());
		//Kunde neu Button an Controller rückmelden
		pnlKundenTyp.getNeuKundeButton().addActionListener(getCtrl());
		//WindowListerner an Controller rückmelden
		this.addWindowListener(getCtrl());
	}

	public Controller getCtrl() {
		return ctrl;
	}

	public void setCtrl(Controller ctrl) {
		this.ctrl = ctrl;
	}

	public RadioButtons getPnlKundenTyp() {
		return pnlKundenTyp;
	}

	public PanelBestellung getPnlBestellung() {
		return pnlBestellung;
	}

	public Kundenliste getPnlKundenListe() {
		return pnlKundenListe;
	}
	/**
	 * Setzen des Image 
	 * @param icon
	 */
	public void setImage(String filename){
		if (filename == null || filename.length() == 0) {
			filename = IconFactory.getDefaultKundenImage();
		}
		image.setText("");
		image.setIcon(IconFactory.getScaledImageIcon(filename, 64, 64));
	}
	
}
