package winedivine.ui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import winedivine.controller.Controller;
import winedivine.model.AVerpackungseinheit;
import winedivine.model.Bestellung;
import winedivine.model.Flasche;
import winedivine.model.Karton;
import winedivine.model.Palette;
import winedivine.model.Weinsorte;
import winedivine.model.weinsorten.WSorten;

/**
 * @author Adil Ajouguim, Michael Schulze und Toan Pham
 *
 */
@SuppressWarnings("serial")
public class BestellDialog extends JFrame implements IConstants {
	private ImageIcon icon;
	private ButtonGroup rbGroup;
	private JRadioButton paletteRB, kartonRB, flascheRB;
	private JLabel weinsorteL;
	@SuppressWarnings("rawtypes")
	private JComboBox weinsorteCB;
	private JLabel anzahlL;
	private JTextField anzahlTF;
	private JLabel preisL;
	private JTextField preisTF;
	private JLabel hinweiseL;
	private JTextField hinweiseTF;

	private String radio; // verpackungseinheit;
	private String weinsorte;
	private int anzahl;
	private float preis;
	private String hinweise = null;

	private JButton okB;

	public JButton getOkB() {
		return okB;
	}

	private JButton abbrechenB;

	private AVerpackungseinheit unit =null;
	private Bestellung bestellung =null;
	
	private Controller ctrl=null;

	public BestellDialog(JFrame fOwner, Controller ctrl) {
		super(GUIBundle.getString("BestellDialog.title"));
		this.ctrl = ctrl;
		this.setIconImage(IconFactory.getImageIcon(IconFactory.APP_ICON).getImage());
		zeichneBestellDialog();
	}

	private void zeichneBestellDialog() {
		this.getContentPane().setLayout(new GridLayout(3, 1));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		this.add(new PKFPanel());
		this.add(new WAPPanel());
		this.add(new HOKAPanel());

		this.setSize(380, 350);
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setVisible(true);
	}

	// Klasse PKFPanel ist Container
	// f�r RadioButtons "Palette", "Karton" und "Flasche(n)"
	class PKFPanel extends JPanel {
		public PKFPanel() {
			this.setLayout(null);

			JLabel leerL = new JLabel("");
			leerL.setIcon(IconFactory.getImageIcon("/icons/question_mark.png"));
			paletteRB = new JRadioButton(GUIBundle.getString("BestellDialog.paletteRB"));
			kartonRB = new JRadioButton(GUIBundle.getString("BestellDialog.kartonRB"));
			flascheRB = new JRadioButton(GUIBundle.getString("BestellDialog.flascheRB"), true);

			leerL.setBounds(new Rectangle(10, 10, 91, 40));
			paletteRB.setBounds(new Rectangle(122, 20, 91, 23));
			kartonRB.setBounds(new Rectangle(122, 45, 91, 23));
			flascheRB.setBounds(new Rectangle(122, 70, 91, 23));

			rbGroup = new ButtonGroup();
			rbGroup.add(paletteRB);
			rbGroup.add(kartonRB);
			rbGroup.add(flascheRB);

			this.add(leerL);
			this.add(paletteRB);
			this.add(kartonRB);
			this.add(flascheRB);

			paletteRB.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent aEvent) {

					radio = "p";
				}
			});

			kartonRB.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent aEvent) {

					radio = "k";
				}
			});

			flascheRB.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent aEvent) {

					radio = "f";
				}
			});
		}
	}

	// Klasse WAPPanel ist Container f�r Labels "Weinsorte", "Anzahl" + "Preis"
	// ComboBox mit Weinsorten und TextFelder Anzahl bzw. Preis
	class WAPPanel extends JPanel {
		@SuppressWarnings("unchecked")
		public WAPPanel() {
			this.setLayout(null);

			weinsorteL = new JLabel(GUIBundle.getString("BestellDialog.weinsorteL"));
			weinsorteL.setBounds(new Rectangle(145, 0, 91, 23));

			weinsorteCB = new JComboBox(WSorten.values());
			weinsorteCB.setBounds(new Rectangle(208, 0, 150, 23));

			anzahlL = new JLabel(GUIBundle.getString("BestellDialog.anzahlL"));
			anzahlL.setBounds(new Rectangle(167, 25, 150, 23));
			anzahlTF = new JTextField("0",5);
			anzahlTF.setBounds(new Rectangle(208, 25, 100, 23));

			preisL = new JLabel(GUIBundle.getString("BestellDialog.preisL"));
			preisL.setBounds(new Rectangle(175, 50, 150, 23));
			preisTF = new JTextField("0.0", 10);
			preisTF.setBounds(new Rectangle(208, 50, 100, 23));

			this.add(weinsorteL);
			this.add(weinsorteCB);
			this.add(anzahlL);
			this.add(anzahlTF);
			this.add(preisL);
			this.add(preisTF);
		}
	}

	// Klasse HOKAPanel ist Container f�r Label "Hinweis",
	// Textfeld, Taste OK und Taste Abbrechen
	class HOKAPanel extends JPanel {
		public HOKAPanel() {

			this.setLayout(null);
			hinweiseL = new JLabel("Hinweise");
			hinweiseL.setBounds(new Rectangle(60, 0, 150, 23));

			hinweiseTF = new JTextField(20);
			hinweiseTF.setBounds(new Rectangle(60, 25, 299, 23));

			okB = new JButton("OK");
			okB.setBounds(new Rectangle(90, 55, 100, 26));
			okB.setPreferredSize(new Dimension(100, 26));
			okB.setActionCommand(IConstants.AC_bestellung_neu);
			okB.addActionListener(ctrl);
			okB.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent aEvent) {
					btnOkPressed();

				}

			});

			abbrechenB = new JButton("Abbrechen");
			abbrechenB.setBounds(new Rectangle(195, 55, 100, 26));
			abbrechenB.setPreferredSize(new Dimension(100, 26));
			abbrechenB.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent aEvent) {
					btnCancelPressed();
				}

			});

			this.add(hinweiseL);
			this.add(hinweiseTF);
			this.add(okB);
			this.add(abbrechenB);
		}
	} // Ende der Klasse HOKAPanel

	public void werteAuslesen(int auswahl) {
		switch (auswahl) {
		case 1:
		case 2:
			weinsorte = (String) weinsorteCB.getSelectedItem();
			break;
		case 3:
			if (anzahlTF.getText().length() > 0) {
				try {
					anzahl = Integer.parseInt(anzahlTF.getText());
				} catch (Exception e) {
					showInfo();
				}
			}
			break;
		case 4:
			if (preisTF.getText().length() > 0) {
				try {
					preis = Float.parseFloat(preisTF.getText());
				} catch (Exception e) {
					showInfo();
				}
			}
			break;
		case 5:
			if (hinweiseTF.getText().length() > 0) {
				try {
					hinweise = hinweiseTF.getText();
				} catch (Exception e) {
				}
			}
			break;
		}
	}

	private void btnOkPressed() {
		if (paletteRB.isSelected()) {
			setUnit(new Palette());
		} else if (kartonRB.isSelected()) {
			setUnit(new Karton());
		} else {
			setUnit(new Flasche());
		}

		bestellung = new Bestellung(Integer.parseInt(anzahlTF.getText()),

				new Weinsorte(((WSorten) weinsorteCB.getSelectedItem()).getWName(),
						((WSorten) weinsorteCB.getSelectedItem()).getWPreis()));

	}

	private void btnCancelPressed() {
		dispose();
	}

	public void showInfo() {
		JOptionPane.showMessageDialog(null, "Bitte geben Sie eine Zahl ein.", "Info", JOptionPane.ERROR_MESSAGE);
	}

	/*
	 * Getter & Setter
	 */
	public AVerpackungseinheit getUnit() {
		return unit;
	}

	public void setUnit(AVerpackungseinheit unit) {
		this.unit = unit;
	}

	public Bestellung getBestellung() {
		return bestellung;
	}

	public void setBestellung(Bestellung bestellung) {
		this.bestellung = bestellung;
	}

}