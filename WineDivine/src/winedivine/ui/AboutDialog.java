package winedivine.ui;

import java.awt.Graphics;
import java.util.Arrays;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AboutDialog extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6257578442029023970L;

	private int cnt = 30;

	/**
	 * 
	 */
	public AboutDialog(WineDivineFrame frame) {
		super(frame);
		this.setTitle(GUIBundle.getString("AboutDialog.title"));
		this.setResizable(false);
		this.setBounds(frame.getWidth() / 2 - 100, frame.getHeight() /2, 500, 80);

		this.add(panel);

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				setVisible(true);
			}
		});

		new Thread(new Runnable() {
			public void run() {
				while (cnt > -700) {
					--cnt;
					panel.repaint();
					try {
						Thread.sleep(25);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				closeDlg();
			}
		}).start();
		
	}
	
	
	private void closeDlg() {
		setVisible(false);
		dispose();
	}

	/**
	 * 
	 */
	JPanel panel = new JPanel() {
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawString(Arrays.toString(getCredits()).replace("[", "").replace("]", ""), cnt, 30);
		}
	};

	/**
	 * 
	 * @return
	 */
	private String[] getCredits() {
		String[] credits = new String[9];

		credits[0] = "Adil Ajougim";
		credits[1] = "Bao Toan Pham";
		credits[2] = "Christian Stirmer";
		credits[3] = "Elena Kuzminykh";
		credits[4] = "Markus Schubert";
		credits[5] = "Michael Schulze";
		credits[6] = "Michael Stegherr";
		credits[7] = "Stephan Wolf";
		credits[8] = "OCPJP 2017 featured by Monika T.";

		return credits;

	}

}
