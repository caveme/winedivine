package winedivine.ui;

import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MenueWeinvertrieb extends JMenuBar implements IConstants {

	// Men�leiste Elemente
	JMenu datei;
	JMenu bestellung;
	JMenu kunde;
	JMenu hilfe;

	// Untermen� Datei
	JMenuItem erfassen;
	JMenuItem oeffnen;
	JMenuItem sichern;
	JMenuItem oeffnenbinaer;
	JMenuItem sichernbinaer;
	JMenuItem beenden;

	// Untermen� Kunde
	// JMenuItem neu;
	JMenuItem einzelV;
	JMenuItem gesellschaft;
	JMenuItem grossV;
	JMenuItem aendern;
	JMenuItem loeschen;

	// Untermen� Hilfe

	JMenuItem autorin;
	JMenuItem info;

	public MenueWeinvertrieb() {
		// Men�elemente erzeugen
		datei = new JMenu(GUIBundle.getString("datei"));
		bestellung = new JMenu(GUIBundle.getString("bestellung"));
		kunde = new JMenu(GUIBundle.getString("kunde"));
		kunde.setActionCommand(AC_kunde);
		hilfe = new JMenu(GUIBundle.getString("hilfe"));
		hilfe.setActionCommand(AC_hilfe);

		// Untermen�
		erfassen = new JMenuItem(GUIBundle.getString("erfassen"));
		erfassen.setActionCommand(AC_bestellung);
		oeffnen = new JMenuItem(GUIBundle.getString("oeffnen"));
		oeffnen.setActionCommand(AC_oeffnen);
		oeffnenbinaer = new JMenuItem(GUIBundle.getString("oeffnenbinaer"));
		sichern = new JMenuItem(GUIBundle.getString("sichern"));
		sichern.setActionCommand(AC_sichern);
		sichernbinaer = new JMenuItem(GUIBundle.getString("sichernbinaer"));
		beenden = new JMenuItem(GUIBundle.getString("beenden"));
		beenden.setActionCommand(AC_beenden);
		// neu = new JMenuItem("Neu");
		// neu.setActionCommand(AC_neu);
		einzelV = new JMenuItem(GUIBundle.getString("einzelV"));
		einzelV.setActionCommand(AC_einzelV);
		gesellschaft = new JMenuItem(GUIBundle.getString("gesellschaft"));
		gesellschaft.setActionCommand(AC_gesellschaft);
		grossV = new JMenuItem(GUIBundle.getString("grossV"));
		grossV.setActionCommand(AC_grossV);
		aendern = new JMenuItem(GUIBundle.getString("aendern"));
		aendern.setActionCommand(AC_aendern);
		loeschen = new JMenuItem(GUIBundle.getString("loeschen"));
		loeschen.setActionCommand(AC_loeschen);
		autorin = new JMenuItem(GUIBundle.getString("autorin"));
		autorin.setActionCommand(AC_autorin);
		info = new JMenuItem(GUIBundle.getString("info"));
		info.setActionCommand(AC_info);

		// Men� hinzuf�gen
		this.add(datei);
		this.add(bestellung);
		this.add(kunde);
		this.add(hilfe);

		// Untermen� zu Datei hinzuf�gen
		datei.add(oeffnen);
		datei.add(sichern);
//		datei.add(oeffnenbinaer);
//		datei.add(sichernbinaer);
		datei.add(beenden);

		bestellung.add(erfassen);

		// Untermen� zu Kunde hinzuf�gen
		// kunde.add(neu);
		kunde.add(einzelV);
		kunde.add(gesellschaft);
		kunde.add(grossV);
		kunde.addSeparator();
		kunde.add(aendern);
		kunde.add(loeschen);

		// Untermen� zu Hilfe hinzuf�gen
		hilfe.add(autorin);
		hilfe.add(info);

	}

	/**
	 * Adds ActionsListener to Components
	 * 
	 * @param al
	 */
	public void addListener(ActionListener al) {
		erfassen.addActionListener(al);
		oeffnen.addActionListener(al);
		sichern.addActionListener(al);
		oeffnenbinaer.addActionListener(al);
		sichernbinaer.addActionListener(al);
		beenden.addActionListener(al);
		// neu.addActionListener(al);
		einzelV.addActionListener(al);
		gesellschaft.addActionListener(al);
		grossV.addActionListener(al);
		aendern.addActionListener(al);
		loeschen.addActionListener(al);
		autorin.addActionListener(al);
		info.addActionListener(al);

	}

	/**
	 * Removes ActionsListener from Components
	 * 
	 * @param al
	 */
	public void removeListener(ActionListener al) {
		erfassen.removeActionListener(al);
		oeffnen.removeActionListener(al);
		sichern.removeActionListener(al);
		oeffnenbinaer.removeActionListener(al);
		sichernbinaer.removeActionListener(al);
		beenden.removeActionListener(al);
		// neu.removeActionListener(al);
		einzelV.removeActionListener(al);
		gesellschaft.removeActionListener(al);
		grossV.removeActionListener(al);
		aendern.removeActionListener(al);
		loeschen.removeActionListener(al);
		autorin.removeActionListener(al);
		info.removeActionListener(al);

	}

}