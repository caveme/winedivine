package winedivine.ui;

import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import winedivine.kunde.AKunde;

public class RadioButtons extends JPanel implements IConstants{
private ButtonGroup group;

	private AKunde neuer=null;
	private JRadioButton einzelV, grossV, gesellschaft;
	private JButton neuKundeButton;
	
	
	public RadioButtons() {
		super();
	einzelV = new JRadioButton(GUIBundle.getString("einzelV"), true);
//	einzelV.setActionCommand(AC_einzelV);
    grossV = new JRadioButton(GUIBundle.getString("grossV"));
//    grossV.setActionCommand(AC_grossV);
//    gesellschaft = new JRadioButton("Gesellschaft mit Liz.");
    gesellschaft = new JRadioButton(GUIBundle.getString("gesellschaft"));
//    gesellschaft.setActionCommand(AC_gesellschaft);
    neuKundeButton= new JButton(GUIBundle.getString("neuKundeButton"));
    neuKundeButton.setActionCommand(AC_neu);
    
    group = new ButtonGroup(); //Group dient dazu, dass nur ein auf einmal eingeclickt werden kann
    group.add(einzelV);
    group.add(grossV);
    group.add(gesellschaft);
    
    this.setLayout(new GridLayout(4,1));
    this.setSize(250,450);
    
    this.add(einzelV);
    this.add(grossV);
    this.add(gesellschaft);
    this.add(neuKundeButton);
    this.setVisible(true);
    
	}
	
	public JButton getNeuKundeButton() {
		return neuKundeButton;
	}

	public AKunde getNeuer() {
		return neuer;
	}

	public void setNeuer(AKunde neuer) {
		this.neuer = neuer;
	}

	public JRadioButton getEinzelV() {
		return einzelV;
	}

	public void setEinzelV(JRadioButton einzelV) {
		this.einzelV = einzelV;
	}

	public JRadioButton getGrossV() {
		return grossV;
	}

	public void setGrossV(JRadioButton grossV) {
		this.grossV = grossV;
	}

	public JRadioButton getGesellschaft() {
		return gesellschaft;
	}

	public void setGesellschaft(JRadioButton gesellschaft) {
		this.gesellschaft = gesellschaft;
	}

	public static void main(String[] args) {
		JFrame frame= new JFrame();
		RadioButtons rb= new RadioButtons();
		frame.getContentPane().setLayout(new GridLayout());
		frame.setSize(200,300);
		frame.setLocation(10,10);
		frame.getContentPane().add(rb);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

	
}


