package winedivine.ui;

import java.awt.BorderLayout;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import winedivine.kunde.AKunde;
import winedivine.model.Bestellung;
import winedivine.model.Weinsorte;

public class PanelBestellung  extends JPanel{
	
	private JTable table;
	private JScrollPane scrollPane; 
	
	private SimpleDateFormat sdf = new SimpleDateFormat(GUIBundle.getString("PanelBestellung.SimpleDateFormat"));
	private NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
	
	private static Object default_rowData[][] = { 
			{ "", "", "", "", "" , "" }
			};
	
	private int anzahl;
	private double gesamtPreis;
	private double gewaehrterRabatt = 0.0;
	private double gesamtPreisNetto;
	private Weinsorte wein;
	
	private static String columnNames[] = { GUIBundle.getString("PanelBestellung.columnNames.Datum"), GUIBundle.getString("PanelBestellung.columnNames.Anzahl"),GUIBundle.getString("PanelBestellung.columnNames.Sorte"), GUIBundle.getString("PanelBestellung.columnNames.Preis"), GUIBundle.getString("PanelBestellung.columnNames.Rabatt"),GUIBundle.getString("PanelBestellung.columnNames.Gesamt") };

	{
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		nf.setGroupingUsed(true);
		nf.setRoundingMode(RoundingMode.HALF_EVEN);
		
	}
	
	public PanelBestellung() {
	    super();
		this.setLayout(new BorderLayout());
		table = //new JTable(default_rowData, columnNames);
		new JTable(default_rowData, columnNames) {
	        private static final long serialVersionUID = 1L;
	        //Editieren der Zellen deaktivieren
	        public boolean isCellEditable(int row, int column) {                
	                return false;               
	        };
	        
	        DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

	        { // initializer block
	            renderRight.setHorizontalAlignment(SwingConstants.RIGHT);
	        }

	        @Override
	        public TableCellRenderer getCellRenderer (int arg0, int arg1) {
	            return renderRight;
	        }
	    };
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//		table.setAutoCreateRowSorter(true);
		scrollPane = new JScrollPane(table);
		this.add(scrollPane, BorderLayout.CENTER);
	}
	
	
	/**
	 * Kann genutzt werden um die Table mit Daten zu f�ttern oder Neu zu erstellen.
	 * @param kundenDaten
	 */
	public void updateTable(AKunde kundenDaten) {
		TreeMap<Date, Bestellung> tmp = kundenDaten.getBestellungsverwaltung().getBestellungsListe();

		DefaultTableModel dtm = new DefaultTableModel(columnNames,0);
		
		
		Set<Date> tmpkey = tmp.keySet();
		
		Iterator<Date> iter = tmpkey.iterator();
		Date nextDate = null;
		Bestellung b = null;
		while (iter.hasNext()) {
		    nextDate = iter.next();
		    b = tmp.get(nextDate);
		    Object[] zeile= new Object[]{
		    		sdf.format(b.getDatum()),
		    		String.format("%5d",b.getAnzahl()),
		    		b.getWein().getWeinsorte(),
		    		nf.format(b.getWein().getPreis()),
		    		nf.format(b.getGewaehrterRabatt()),
		    		nf.format((double)Math.round(b.getGesamtPreis()*100)/100)
		    		};

		    dtm.addRow(zeile);
		}
		this.table.setModel(dtm);
	}
	
}