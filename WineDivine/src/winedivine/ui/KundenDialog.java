package winedivine.ui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import winedivine.kunde.AKunde;
import winedivine.kunde.Adresse;
import winedivine.kunde.Endverbraucher;
import winedivine.kunde.GesellschaftMLiz;
import winedivine.kunde.Grossverbraucher;

public class KundenDialog extends JFrame implements ActionListener, IConstants {
	private static final long serialVersionUID = -3498730926316597545L;

	private ButtonGroup group;
	private JTextField txtFName, txtFstrasse, txtFOrt, txtFPlz, txtFRabatt, txtFVert, txtFStat;
	private JRadioButton rButBonJa, rButBonNein;
	private JButton butNeuK, butCancel;
	private JComboBox<Object> picBox;
	private JFrame self = this;
	JLabel picLabel;
	private AKunde kunde;

	public KundenDialog(AKunde kunde) {
		super();
		this.setTitle(GUIBundle.getString("KundenDialog.kdlgTitle"));
		this.getContentPane().setLayout(new FlowLayout());
		this.kunde = kunde;

		// �berschrift
		String topLabelTxt =  GUIBundle.getString("KundenDialog.kdlgMsg").replace("REPLACE", kunde.getClass().getSimpleName())  ;
		JLabel topLabel = new JLabel(topLabelTxt, JLabel.CENTER);
		this.getContentPane().add(topLabel);

		// Eingabefelder
		txtFName = new JTextField(GUIBundle.getString("KundenDialog.txtFName"), 20);
		txtFName.setToolTipText(GUIBundle.getString("KundenDialog.txtFName"));
		txtFstrasse = new JTextField(GUIBundle.getString("KundenDialog.txtFstrasse"), 20);
		txtFOrt = new JTextField(GUIBundle.getString("KundenDialog.txtFOrt"), 20);
		txtFPlz = new JTextField(GUIBundle.getString("KundenDialog.txtFPlz"), 20);
		txtFRabatt = new JTextField(GUIBundle.getString("KundenDialog.txtFRabatt"), 20);

		this.getContentPane().add(txtFName);

		this.getContentPane().add(txtFstrasse);
		this.getContentPane().add(txtFPlz);
		this.getContentPane().add(txtFOrt);
		this.getContentPane().add(txtFRabatt);

		// Kunden Spezifische Abfrage
		if (this.kunde instanceof GesellschaftMLiz) {
			txtFVert = new JTextField(GUIBundle.getString("KundenDialog.txtFVert"), 20);
			this.getContentPane().add(txtFVert);
		} else if (this.kunde instanceof Endverbraucher) {
			rButBonJa = new JRadioButton(GUIBundle.getString("KundenDialog.rButBonJa"), true);
			rButBonNein = new JRadioButton(GUIBundle.getString("KundenDialog.rButBonNein"));
			group = new ButtonGroup();
			group.add(rButBonJa);
			group.add(rButBonNein);
			this.getContentPane().add(rButBonJa);
			this.getContentPane().add(rButBonNein);
		} else if (this.kunde instanceof Grossverbraucher) {
			txtFStat = new JTextField(GUIBundle.getString("KundenDialog.txtFStat"), 20);
			this.getContentPane().add(txtFStat);
		} else {
			JOptionPane.showMessageDialog(this, GUIBundle.getString("KundenDialog.wrongKundenTyp"));
		}

		// Buttons
		butNeuK = new JButton(GUIBundle.getString("KundenDialog.butNeuK"));
		butCancel = new JButton(GUIBundle.getString("KundenDialog.butCancel"));
		this.getContentPane().add(butNeuK);
		this.getContentPane().add(butCancel);
		butNeuK.setActionCommand(AC_kunde_angelegt);
		butNeuK.addActionListener(this);
		butCancel.addActionListener(this);

		ArrayList<String> fileStringArray = new ArrayList<String>();
		fileStringArray.add("Bild ausw�hlen");

		// File file = new File(IconFactory.IMAGE_PATH);
		// File[] fileList = file.listFiles();
		// for (File f : fileList){
		//
		// if (f. isDirectory()){
		// continue;
		// }
		// fileStringArray.add(f.getName());
		// }

		fileStringArray.add(IconFactory.KundenImage.A.getName());
		fileStringArray.add(IconFactory.KundenImage.B.getName());
		fileStringArray.add(IconFactory.KundenImage.C.getName());
		fileStringArray.add(IconFactory.KundenImage.D.getName());
		fileStringArray.add(IconFactory.KundenImage.E.getName());
		fileStringArray.add(IconFactory.KundenImage.F.getName());
		fileStringArray.add(IconFactory.KundenImage.G.getName());
		fileStringArray.add(IconFactory.KundenImage.H.getName());
		fileStringArray.add(IconFactory.KundenImage.I.getName());

		picBox = new JComboBox<Object>(fileStringArray.toArray());
		picBox.addActionListener(this);
		this.getContentPane().add(picBox);

		picLabel = new JLabel("", JLabel.CENTER);
		this.getContentPane().add(picLabel);

		if (kunde != null) {

			if (kunde.getAnschrift() != null) {
				txtFName.setText(kunde.getAnschrift().getName());
				txtFstrasse.setText(kunde.getAnschrift().getStrasse_hnr());
				txtFOrt.setText(kunde.getAnschrift().getOrt());
				txtFPlz.setText(kunde.getAnschrift().getPlz());
			}
			txtFRabatt.setText(String.valueOf(kunde.getRabatt()));
			picBox.setSelectedItem(kunde.getPicName());

		}
		this.setIconImage(IconFactory.getImageIcon(IconFactory.APP_ICON).getImage());
		this.setSize(300, 330);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setAlwaysOnTop(true);
		this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
	}

	public JButton getKundeAngeButt() {
		return butNeuK;
	}

	public void makeNewKunde() {
		Adresse ad = new Adresse(txtFName.getText(), txtFstrasse.getText(), txtFPlz.getText(), txtFOrt.getText());
		kunde.setAnschrift(ad);

		try {
			kunde.setRabatt(Integer.parseInt(txtFRabatt.getText()));
		} catch (NumberFormatException x) {
			JOptionPane.showMessageDialog(this, GUIBundle.getString("KundenDialog.wrongRabattFormat"));
			return;
		}
		if (this.kunde instanceof GesellschaftMLiz) {
			try {
				((GesellschaftMLiz) this.kunde).setVertrieb(Integer.parseInt(txtFVert.getText()));
			} catch (NumberFormatException x) {
				JOptionPane.showMessageDialog(this, GUIBundle.getString("KundenDialog.wrongVertriebFormat"));
				return;
			}
		} else if (this.kunde instanceof Endverbraucher) {
			((Endverbraucher) this.kunde).setBonitaet(rButBonJa.isSelected());
		} else if (this.kunde instanceof Grossverbraucher) {
			try {
				((Grossverbraucher) this.kunde).setStatus(Integer.parseInt(txtFStat.getText()));
			} catch (NumberFormatException x) {
				JOptionPane.showMessageDialog(this, GUIBundle.getString("KundenDialog.wrongStatusFormat"));
				return;
			}
		} else {
			JOptionPane.showMessageDialog(this, GUIBundle.getString("KundenDialog.wrongKundenTyp"));
			return;
		}

		self.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(picBox)) {
			if (picBox.getSelectedIndex() == 0) {
				return;
			}
			kunde.setPicName((String) picBox.getSelectedItem());
			ImageIcon icon = IconFactory.getImageIcon((String) picBox.getSelectedItem());
			picLabel.setIcon(icon);
		}
		if (e.getSource().equals(butNeuK)) {
			makeNewKunde();
		}
		if (e.getSource().equals(butCancel)) {
			self.dispose();
		}
	}

}
