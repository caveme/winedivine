package winedivine.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
//import javax.swing.event.ListSelectionEvent;
//import javax.swing.event.ListSelectionListener;

import winedivine.controller.Serialize;
import winedivine.kunde.AKunde;
import winedivine.kunde.Endverbraucher;
import winedivine.kunde.GesellschaftMLiz;
import winedivine.kunde.Grossverbraucher;
import winedivine.model.Flasche;
import winedivine.model.Weinsorte;
import winedivine.model.weinsorten.WSorten;

/**
 *
 * 18.07.2017 Project : winedivine �2017
 *
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class Kundenliste extends JPanel implements MouseListener, Serializable {

	/**
	 * Generate SerialUID
	 */
	private static final long serialVersionUID = 6758007634974564053L;
	private JList jList;
	private DefaultListModel defaultListModel;
	private JScrollPane jScrollPane;
	private JPanel pnlHead;
	private ArrayList<AKunde> listeMitKunden;
	private int itemIndex;

	public Kundenliste() {
		this(new ArrayList<AKunde>());
	}

	public Kundenliste(ArrayList<AKunde> listeMitKunden) {
		super();
		this.setLayout(new BorderLayout());
		this.pnlHead = new JPanel(new FlowLayout(FlowLayout.CENTER));
		this.add(pnlHead, BorderLayout.NORTH);
		this.pnlHead.add(new JLabel(GUIBundle.getString("pnlKundenliste")));

		this.listeMitKunden = listeMitKunden;
		/**
		 * Zum Testen, falls keine Kundendaten vorhanden sind.
		 */
		// if (listeMitKunden.size() == 0 && Serialize
		// .fileExists(Serialize.FileNames.KUNDENLISTE.dateiName)) {
		// this.listeMitKunden = (ArrayList<AKunde>) Serialize
		// .loadFromFile(Serialize.FileNames.KUNDENLISTE.dateiName);
		// } else if (listeMitKunden.size() == 0) {
		// Endverbraucher e1 = new Endverbraucher(1.55, true);
		// Endverbraucher e2 = new Endverbraucher(4.55, true);
		// Endverbraucher e3 = new Endverbraucher(5.55, true);
		// e1.getBestellungsverwaltung().neuBestellung(new Weinsorte("Grauburgunder",
		// 2.55), new Flasche(), 5);
		// Grossverbraucher g1 = new Grossverbraucher(2.55, 1);
		// Grossverbraucher g2 = new Grossverbraucher(2.55, 12);
		// Grossverbraucher g3 = new Grossverbraucher(2.55, 123);
		// GesellschaftMLiz gl1 = new GesellschaftMLiz(2.55, 1);
		// GesellschaftMLiz gl2 = new GesellschaftMLiz(2.55, 12);
		// GesellschaftMLiz gl3 = new GesellschaftMLiz(2.55, 123);
		//
		// this.listeMitKunden.add(e1);
		// this.listeMitKunden.add(e2);
		// this.listeMitKunden.add(e3);
		// this.listeMitKunden.add(g1);
		// this.listeMitKunden.add(g2);
		// this.listeMitKunden.add(g3);
		// this.listeMitKunden.add(gl1);
		// this.listeMitKunden.add(gl2);
		// this.listeMitKunden.add(gl3);
		// }

		defaultListModel = new DefaultListModel();

		for (AKunde aKunde : this.listeMitKunden) {
			defaultListModel.addElement(aKunde);
		}

		this.setjList(new JList(defaultListModel));
		this.getjList().setFont(new Font("Courier New", Font.BOLD, 13));
		this.getjList().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getjList().setVisibleRowCount(3);
		this.getjList().setFont(new Font("Courier New", Font.BOLD, 12));
		// this.getjList().addListSelectionListener(this);
		this.getjList().addMouseListener(this);

		jScrollPane = new JScrollPane(this.getjList());
		this.add(jScrollPane, BorderLayout.CENTER);

	}

	public JList getjList() {
		return jList;
	}

	public void setjList(JList jList) {
		this.jList = jList;
	}

	public void addItem(AKunde kunde) {
		if (listeMitKunden.contains(kunde)) {
			listeMitKunden.remove(kunde);
			defaultListModel.removeElement(kunde);
		}

		listeMitKunden.add(kunde);
		defaultListModel.addElement(kunde);
	}

	public AKunde getSelectedItem() {
		if (getItemIndex() == -1 || listeMitKunden.isEmpty())
			return null;
		return (AKunde) listeMitKunden.get(getItemIndex());
	}

	public int getItemIndex() {
		return itemIndex;
	}

	public void setItemIndex(int itemIndex) {
		/**
		 * TODO: Notify Controller
		 */
		this.itemIndex = itemIndex;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	/**
	 * Bietet die Funktion eintr�ge aus der ArrayList zu l�schen.
	 *
	 * TODO: L�schanweisung muss noch an die Aussenstehende Liste �bergeben werden,
	 * derzeit alles nur in der View
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.isControlDown()) {
			int index = this.getjList().getSelectedIndex();
			removeItem(index);
		}
	}

	/*
	 * Entfernt das ausgewählte Item aus der Liste
	 */
	public void removeItem(int index) {
		if (index != -1) {
			this.listeMitKunden.remove(index);
			defaultListModel.removeElementAt(index);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	public ArrayList<AKunde> getListeMitKunden() {
		return listeMitKunden;
	}

	public void setListeMitKunden(ArrayList<AKunde> listeMitKunden) {
		if (listeMitKunden != null) {

			this.listeMitKunden = listeMitKunden;

			defaultListModel = new DefaultListModel();
			getjList().setModel(defaultListModel);

			for (AKunde aKunde : this.listeMitKunden) {
				defaultListModel.addElement(aKunde);
			}
		}
	}

}
