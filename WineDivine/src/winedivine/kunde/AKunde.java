package winedivine.kunde;

import java.io.Serializable;

import winedivine.model.Bestellungsverwaltung;
import winedivine.ui.GUIBundle;

public abstract class AKunde implements Serializable {
	// Attribute
	private double rabatt;
	private Adresse anschrift;
	private Integer kundNr;
	private String picName;
	private Bestellungsverwaltung bestellungsverwaltung = new Bestellungsverwaltung(this);

	// Konstruktoren
	public AKunde() {
		if (this.bestellungsverwaltung == null) {
			this.bestellungsverwaltung = new Bestellungsverwaltung(this);
		}
	}

	public AKunde(double rabatt) {
		super();
		this.setRabatt(rabatt);
	}

	// INIT
	{
		this.rabatt = 0.0;
		this.kundNr = KundenNummernVergeber.getExample().getKundenNummer();
		this.picName = "";
	}

	// Methoden
	/**
	 * old method
	 */
	// @Override
	// public String toString() {
	// String retStr;
	// retStr = String.format("%-17s %d\n%-17s %.2f%%\n%s \n%s\n",
	// "Kunden Nummer:",
	// this.kundNr,
	// "Rabatt:",
	// (float)this.rabatt,
	// "Adresse:",
	// this.anschrift);
	// return retStr;
	// }

	@Override
	public String toString() {
		String retStr = "";

		if (this instanceof Endverbraucher) {
			retStr = String.format("%-12s %-20s", GUIBundle.getString("clienttype"), this.getClass().getSimpleName());
			retStr += String.format("%-10s %-10d", GUIBundle.getString("clientnumber"), this.getKundNr());
			retStr += String.format("%-12s %-4s", GUIBundle.getString("creditworthness"),
					((Endverbraucher) this).isBonitaet());
		} else if (this instanceof Grossverbraucher) {
			retStr = String.format("%-12s %-20s", GUIBundle.getString("clienttype"), this.getClass().getSimpleName());
			retStr += String.format("%-10s %-10d", GUIBundle.getString("clientnumber"), this.getKundNr());
			retStr += String.format("%-12s %04d", GUIBundle.getString("status"), ((Grossverbraucher) this).getStatus());
		} else if (this instanceof GesellschaftMLiz) {
			retStr = String.format("%-12s %-20s", GUIBundle.getString("clienttype"), this.getClass().getSimpleName());
			retStr += String.format("%-10s %-10d", GUIBundle.getString("clientnumber"), this.getKundNr());
			retStr += String.format("%-12s %04d", GUIBundle.getString("distribution"),
					((GesellschaftMLiz) this).getVertrieb());
		}

		retStr += String.format("%12s %-7.2f", GUIBundle.getString("discount"), this.getRabatt());
		retStr += String.format("%-10s %-50s", GUIBundle.getString("address"), this.getAnschrift());
		return retStr;
	}

	// Getter und Setter
	public double getRabatt() {
		return rabatt;
	}

	public void setRabatt(double rabatt) {
		this.rabatt = rabatt;
	}

	public Adresse getAnschrift() {
		return anschrift;
	}

	public void setAnschrift(Adresse anschrift) {
		this.anschrift = anschrift;
	}

	public Integer getKundNr() {
		return kundNr;
	}

	public void setKundNr(Integer kundNr) {
		this.kundNr = kundNr;
	}

	public String getPicName() {
		return picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public Bestellungsverwaltung getBestellungsverwaltung() {
		return bestellungsverwaltung;
	}

	public void setBestellungsverwaltung(Bestellungsverwaltung bestellungsverwaltung) {
		this.bestellungsverwaltung = bestellungsverwaltung;
	}
}
