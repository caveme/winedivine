package winedivine.kunde;

import java.io.Serializable;

public class Adresse implements Serializable{

	private String name = "";
	private String strasse_hnr = "";
	private String plz = "";
	private String ort = "";

	public Adresse(String name, String strasseHnr, String plz, String ort) {
		this.name = name;
		this.strasse_hnr = strasseHnr;
		this.plz = plz;
		this.ort = ort;
	}
	
	public Adresse(){
		
	}

	@Override
	public String toString() {
		String result = this.name + ", \n" + this.strasse_hnr + ", \n" + this.plz + " " + this.ort;
		return result;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStrasse_hnr() {
		return strasse_hnr;
	}

	public void setStrasse_hnr(String strasse_hnr) {
		this.strasse_hnr = strasse_hnr;
	}

	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

}
