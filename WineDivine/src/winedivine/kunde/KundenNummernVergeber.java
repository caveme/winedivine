package winedivine.kunde;

public class KundenNummernVergeber {

    
    /**
     * test nochmal elena
     */
    private int nummer = 1;
    private static KundenNummernVergeber kv = null;

    /**
     * Factory Methode f�r die Klasse
     * 
     * @return Eine Instanz der Klasse
     */
    public static KundenNummernVergeber getExample() {
	if (KundenNummernVergeber.kv == null) {
	    KundenNummernVergeber.kv = new KundenNummernVergeber();
	}
	return kv;
    }

    /**
     * Private da als Singleton Realisiert. Bitte die getExample Methode nutzen
     * um eine Intanz zu erhalten
     */
    private KundenNummernVergeber() {
	super();
    }

    @Override
    public String toString() {
        return "Erstellt eine Zuf�llige Kundennummer";
    }
    
    /**
     * 
     * @return Liefert eine Zufallszahl zwischen 1 und 1000
     */
    public int getKundenNummer() {
	this.nummer = (int) ((Math.random() * 900) + 100);
	return this.nummer;
    }

}
