package winedivine.kunde;

import java.io.Serializable;

public class Grossverbraucher extends AKunde implements Serializable {

	// Attribute
	private int status;
	
	// Konstruktoren
	public Grossverbraucher(){		
	}
	public Grossverbraucher(double rabatt, int status){
		super(rabatt);
		this.status = status;
	}
	
	// Methoden
	/**
	 * old methode
	 */
//	@Override
//	public String toString() {
//		String retStr;
//		retStr = String.format("%-17s %s\n%-17s %d\n",
//							   "Kunden Art:",
//							   "Grossverbraucher",
//							   "Vertrieb:",
//							   this.status);
//		retStr += super.toString();
//		return retStr;
//	}
	
	@Override
	public String toString() {
	    return super.toString();
	}
	
	// Getter und Setter
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
