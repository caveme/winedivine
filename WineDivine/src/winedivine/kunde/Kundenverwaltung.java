package winedivine.kunde;

import java.util.ArrayList;
import java.util.Comparator;

public class Kundenverwaltung implements Comparator<AKunde>{
	// Attribute
	private ArrayList<AKunde> kundenListe = new ArrayList<AKunde>();
	
	// Konstruktoren
	public Kundenverwaltung(){		
	}
	
	// Methoden	
	public void addKunde(AKunde k) {
		this.kundenListe.add(k);
	}
	public void removeKunde(AKunde k) {
		this.kundenListe.remove(k);
	}	
	public AKunde newGrossverbraucher(){
		return new Grossverbraucher();
	}
	
	public AKunde newEndverbraucher(){
		return new Endverbraucher();
	}
	/*
	 * Getter Setter 
	 */
	public ArrayList<AKunde> getKundenListe() {
		return kundenListe;
	}

	public void setKundenListe(ArrayList<AKunde> kundenListe) {
		this.kundenListe = kundenListe;
	}

	public AKunde newGesellschaftMLiz(){
		return new GesellschaftMLiz();
	}
	
	@Override
	public int compare(AKunde k1, AKunde k2) {
		if (k1.getKundNr() == k2.getKundNr()){
			return 0;
		}else if (k1.getKundNr() < k2.getKundNr()){
			return -1;
		}else{
			return 1;
		}
	}
	
	@Override
	public String toString() {
		String retStr = "";
		for (AKunde kunde : kundenListe) {
			retStr += kunde.toString();
			retStr += "\n----------------------------------\n";
		}
		return retStr;
	}

}
