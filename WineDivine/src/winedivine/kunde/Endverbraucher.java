package winedivine.kunde;

import java.io.Serializable;

public class Endverbraucher extends AKunde implements Serializable {

  		private boolean bonitaet;
		
		public Endverbraucher(){
		    super();
		}
		
		public Endverbraucher(double rabatt, boolean bonitaet){
			super(rabatt);
			this.bonitaet = bonitaet;
		}
		
		public boolean isBonitaet() {
			return bonitaet;
		}
		
		public void setBonitaet(boolean bonitaet) {
			this.bonitaet = bonitaet;
		}
		
		/**
		 * Old method
		 */
//		@Override
//		public String toString() {
//			String retStr;
//			retStr = String.format("%-17s %s\n%-17s %b\n",
//								   "Kunden Art:",
//								   "Endverbraucher",
//								   "bonitaet:",
//								   this.bonitaet);
//			retStr += super.toString();
//			return retStr;
//		}
		
		@Override
		public String toString() {
		    return super.toString();
		}
}
