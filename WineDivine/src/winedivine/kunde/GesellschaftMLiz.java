package winedivine.kunde;

import java.io.Serializable;

public class GesellschaftMLiz extends AKunde implements Serializable {
	
	// Attribute
	private int vertrieb;
	
	// Konstruktoren
	public GesellschaftMLiz(){		
	}
	public GesellschaftMLiz(double rabatt, int vertrieb){
		super(rabatt);
		this.vertrieb = vertrieb;
	}
	
	// Methoden
	/**
	 * old methode
	 */
//	@Override
//	public String toString() {
//		String retStr;
//		retStr = String.format("%-17s %s\n%-17s %d\n",
//							   "Kunden Art:",
//							   "GesellschaftMLiz",
//							   "Vertrieb:",
//							   this.vertrieb);
//		retStr += super.toString();
//		return retStr;
//	}
	
	@Override
	public String toString() {
	    return super.toString();
	}
	
	// Getter und Setter
	public int getVertrieb() {
		return vertrieb;
	}
	public void setVertrieb(int vertrieb) {
		this.vertrieb = vertrieb;
	}
	
}
