package winedivine.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

import winedivine.kunde.AKunde;
import winedivine.kunde.Adresse;
import winedivine.kunde.Endverbraucher;
import winedivine.kunde.GesellschaftMLiz;
import winedivine.kunde.Grossverbraucher;
import winedivine.model.Bestellung;
import winedivine.model.Weinsorte;
import winedivine.ui.GUIBundle;

public class BestellungPersistor extends APersistor<Bestellung, Collection<Bestellung>, Integer> {
	private static boolean result = false;

	private static PreparedStatement pstmt;
	private static ResultSet rs;
	private static ResultSetMetaData rsmd;
	private static String sql;

	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			DataBaseUtil.getString("Bestellung.SimpleDateFormat"));

	private final static String KUNDE_NULL_ERROR = "Kunde/Bestellung ist NULL - speichern nicht möglich!";
	private final static String KUNDE_NOT_FOUND_ERROR = "Bestellungen zu Kunde KNR konnten nicht gefunden werden!";
	private final static String BEST_DELETE_ERROR = "Bestellung konnte nicht gelöscht werden!";
	private final static String BEST_SAVE_SUCCESS = "Bestellung erfolgreich gespeichert!";
	private final static String BEST_UPDATE_SUCCESS = "Bestellung erfolgreich aktualisiert!";
	private final static String BEST_DELETE_SUCCESS = "Bestellung erfolgreich gelöscht!";

	/*
	 * (non-Javadoc)
	 * 
	 * @see winedivine.database.APersistor#save(java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean save(Bestellung bestellung, Integer kundennummer) {
		try {
			if (kundennummer == null || bestellung == null)
				throw new RuntimeException(KUNDE_NULL_ERROR);
			else
				result = performInsert(bestellung, kundennummer);
			if (DataBaseUtil.isDebug_on())
				System.out.println(BEST_SAVE_SUCCESS);
		} catch (SQLException insertException) {
			try {
				if (insertException.getErrorCode() == 19) {

					result = performUpdate(bestellung, kundennummer);
					if (DataBaseUtil.isDebug_on())
						System.out.println(BEST_UPDATE_SUCCESS);
				} else {
					System.out.println(insertException.getErrorCode() + " : " + insertException.getLocalizedMessage());
					insertException.printStackTrace();
				}
			} catch (SQLException updateException) {
				System.out.println(updateException.getErrorCode() + " : " + updateException.getLocalizedMessage());
				updateException.printStackTrace();
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see winedivine.database.APersistor#save(java.util.Collection,
	 * java.lang.Object)
	 */
	@Override
	public boolean save(Collection<Bestellung> objectList, Integer key) {
		result = true;
		if (objectList != null) {

			for (Bestellung bestellung : objectList) {
				if (bestellung != null)
					result = result && save(bestellung, key);
			}

		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see winedivine.database.APersistor#readAll(java.lang.Object)
	 */
	@Override
	public ArrayList<Bestellung> readAll(Integer kundennummer) {
		ArrayList<Bestellung> bestellungen = new ArrayList<Bestellung>();
		try {
			if (kundennummer != null) {
				sql = "SELECT * FROM BESTELLUNG WHERE KUNDENNR = ? ;";
				if (DataBaseUtil.isDebug_on())
					System.out.println(sql);

				pstmt = DataBaseUtil.getConnection().prepareStatement(sql);
				pstmt.setInt(1, kundennummer);
				rs = pstmt.executeQuery();
				rsmd = rs.getMetaData();

				while (rs.next()) {

					Bestellung bestellung = new Bestellung(rs.getInt("Anzahl"),
							new Weinsorte(rs.getString("WeinSorte"), rs.getDouble("NettoPreis")),
							sdf.parse(rs.getString("Datum")));
					bestellung.setGesamtPreisNetto(rs.getDouble("NettoPreis"));
					bestellung.setGesamtPreis(rs.getDouble("GesamtPreis"));
					bestellung.setGewaehrterRabatt(rs.getDouble("GewRabatt"));
					bestellungen.add(bestellung);
				}
			}
		} catch (SQLException | ParseException e) {
			e.printStackTrace();
		}

		return bestellungen;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see winedivine.database.APersistor#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Integer kundennummer) {
		try {
			if (kundennummer != null) {
				result = performDelete(kundennummer);
				if (DataBaseUtil.isDebug_on())
					System.out.println(BEST_DELETE_SUCCESS);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(BEST_DELETE_ERROR);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see winedivine.database.APersistor#performDelete(java.lang.Object)
	 */
	@Override
	public boolean performDelete(Integer kundennummer) throws SQLException {

		sql = "DELETE FROM BESTELLUNG ;";
		if (kundennummer != null) {
			sql = sql.replace("BESTELLUNG", " BESTELLUNG WHERE KundenNr = ? ");
			pstmt = DataBaseUtil.getConnection().prepareStatement(sql);
			pstmt.setInt(1, kundennummer);
		}
		if (DataBaseUtil.isDebug_on())
			System.out.println(sql);

		return pstmt.executeUpdate() > 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see winedivine.database.APersistor#performUpdate(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public boolean performUpdate(Bestellung bestellung, Integer kundennummer) throws SQLException {
		sql = "UPDATE BESTELLUNG " + "SET WeinSorte = ?, Anzahl = ?, NettoPreis = ?, GewRabatt = ?, GesamtPreis = ? "
				+ "WHERE KundenNr = ? AND Datum = ?";
		if (DataBaseUtil.isDebug_on())
			System.out.println(sql);

		pstmt = DataBaseUtil.getConnection().prepareStatement(sql);

		pstmt.setString(1, bestellung.getWein().getWeinsorte());
		pstmt.setInt(2, bestellung.getAnzahl());
		pstmt.setDouble(3, bestellung.getGesamtPreisNetto());
		pstmt.setDouble(4, bestellung.getGewaehrterRabatt());
		pstmt.setDouble(5, bestellung.getGesamtPreis());
		pstmt.setString(6, sdf.format(bestellung.getDatum()));
		pstmt.setInt(7, kundennummer);

		return pstmt.executeUpdate() > 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see winedivine.database.APersistor#performInsert(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public boolean performInsert(Bestellung bestellung, Integer kundennummer) throws SQLException {
		sql = "INSERT INTO BESTELLUNG ( KundenNr, Datum, WeinSorte, Anzahl, NettoPreis, GewRabatt, GesamtPreis ) "
				+ "VALUES ( ?, ?, ?, ?, ?, ?, ?  ) ;";
		if (DataBaseUtil.isDebug_on())
			System.out.println(sql);

		pstmt = DataBaseUtil.getConnection().prepareStatement(sql);
		pstmt.setInt(1, kundennummer);
		pstmt.setString(2, sdf.format(bestellung.getDatum()));
		pstmt.setString(3, bestellung.getWein().getWeinsorte());
		pstmt.setInt(4, bestellung.getAnzahl());
		pstmt.setDouble(5, bestellung.getGesamtPreisNetto());
		pstmt.setDouble(6, bestellung.getGewaehrterRabatt());
		pstmt.setDouble(7, bestellung.getGesamtPreis());

		return pstmt.executeUpdate() > 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see winedivine.database.APersistor#read()
	 */
	@Override
	public Collection<Bestellung> read() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see winedivine.database.APersistor#read(java.lang.Object)
	 */
	@Override
	public Bestellung read(Integer Key) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see winedivine.database.APersistor#save(java.util.Collection)
	 */
	@Override
	public boolean save(Collection<Bestellung> objectList) {
		return false;
	}

}
