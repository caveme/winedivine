package winedivine.database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ResourceBundle;

import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteConnection;
import org.sqlite.SQLiteDataSource;
import org.sqlite.SQLiteJDBCLoader;
import org.sqlite.SQLiteOpenMode;

public class DataBaseUtil {

	private static final ResourceBundle bundle = ResourceBundle.getBundle("winedivine.database.dbconnect");

	private static Connection con;
	private static Statement stmt;
	private static ResultSet rs;
	private static ResultSetMetaData rsmsd;

	private static String sql;

	private static boolean dbvalid = false;

	private static boolean debug_on = false;

	/**
	 * Liefert eine Connection zu Datenbank.
	 * 
	 * Selbige ist in der Datei dbconnect.properties definiert!
	 * 
	 * @return Connection
	 */
	public static Connection getConnection() {
		try {
			// sofern keine Connection erzeugt und geöffnet ist,
			// wird eine Connection zur DB geöffnet
			if (con == null || con.isClosed()) {
				Class.forName(getString("DBDriver"));
				con = DriverManager.getConnection(getString("DBConnection"));
				if (DataBaseUtil.isDebug_on())
					System.out.println("Opened database successfully");
			}

		} catch (Exception e) {
			if (e instanceof SQLException)
				System.err.println(((SQLException) e).getErrorCode() + ": " + e.getMessage());

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}

		return con;

	}

	/**
	 * Legt eine Tabelle (COMPANY)
	 * 
	 * @return true, wenn das executeUpdate(sql) einen Wert > 0 liefert
	 */
	public static void validateDB() {
		if (DataBaseUtil.isDebug_on())
			System.out.println("validateDB()");
		try {
			stmt = getConnection().createStatement();
			rs = stmt.executeQuery(
					"SELECT COUNT(*) as Anzahl FROM SQLITE_MASTER WHERE TYPE = 'table' AND  TBL_NAME = 'KUNDE'");
			if (rs.getInt("Anzahl") == 0) {
				dbvalid = stmt.executeUpdate(dbCreateSql) > 0;
				stmt.close();
				if (DataBaseUtil.isDebug_on())
					System.out.println("Table created successfully");
			}
		} catch (Exception e) {
			if (e instanceof SQLException)
				System.err.println(((SQLException) e).getErrorCode() + ": " + e.getMessage());

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}

	}

	/**
	 * init DB
	 */
	public static void initDB() {
		try {
			readNexecuteUpdate(dbCreateSql);
		} catch (IOException | SQLException e) {
			if (e instanceof SQLException)
				System.err.println(((SQLException) e).getErrorCode() + ": " + e.getMessage());

			e.printStackTrace();
		}

	}

	/**
	 * Legt eine Tabelle (COMPANY)
	 * 
	 * @return true, wenn das executeUpdate(sql) einen Wert > 0 liefert
	 */
	public static boolean createTable() {
		boolean result = false;
		System.out.println("createTable()");
		try {

			stmt = getConnection().createStatement();

			if (stmt.execute("SELECT COUNT(*) FROM SQLITE_MASTER WHERE TYPE = 'table' AND  TBL_NAME = 'COMPANY'")) {
				stmt.execute("DROP TABLE COMPANY");
				if (DataBaseUtil.isDebug_on())
					System.out.println("Table dropped successfully");
			}

			String sql = "CREATE TABLE COMPANY " + "(ID INT PRIMARY KEY     NOT NULL,"
					+ " NAME           TEXT    NOT NULL, " + " AGE            INT     NOT NULL, "
					+ " ADDRESS        CHAR(50), " + " SALARY         REAL)";
			result = stmt.executeUpdate(sql) > 0;
			stmt.close();
			System.out.println("Table created successfully");
		} catch (Exception e) {
			if (e instanceof SQLException)
				System.err.println(((SQLException) e).getErrorCode() + ": " + e.getMessage());

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return result;
	}

	/**
	 * Fügt Werte in die Tabelle ein
	 * 
	 * @return true, wenn das executeUpdate(sql) einen Wert > 0 liefert
	 */
	public static boolean insertIntoTable() {
		boolean result = false;
		System.out.println("insertIntoTable()");

		try {
			getConnection().setAutoCommit(false);

			stmt = getConnection().createStatement();
			String sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
					+ "VALUES (1, 'Paul', 32, 'California', 20000.00 );";
			stmt.executeUpdate(sql);

			sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " + "VALUES (2, 'Allen', 25, 'Texas', 15000.00 );";
			stmt.executeUpdate(sql);

			sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " + "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );";
			stmt.executeUpdate(sql);

			sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
					+ "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );";

			result = stmt.executeUpdate(sql) > 0;

			stmt.close();
			getConnection().commit();
			if (DataBaseUtil.isDebug_on())
				System.out.println("Records created successfully");
		} catch (Exception e) {
			if (e instanceof SQLException)
				System.err.println(((SQLException) e).getErrorCode() + ": " + e.getMessage());

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return result;
	}

	/**
	 * Aktualisiert Werte ( für ID = 1 )
	 * 
	 * @return true, wenn das executeUpdate(sql) einen Wert > 0 liefert
	 */
	public static boolean updateTable() {
		boolean result = false;
		System.out.println("updateTable()");

		try {
			getConnection().setAutoCommit(false);

			stmt = getConnection().createStatement();
			String sql = "UPDATE COMPANY set SALARY = 25000.00 where ID=1;";
			result = stmt.executeUpdate(sql) > 0;
			getConnection().commit();

			selectFromAnyTable("COMPANY", true);

			stmt.close();
			if (DataBaseUtil.isDebug_on())
				System.out.println("Update done successfully");
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return result;

	}

	/**
	 * Löscht Werte aus der Tabelle (hier für ID = 2)
	 */
	public static void deleteFromTable() {
		System.out.println("deleteFromTable()");
		try {

			getConnection().setAutoCommit(false);

			stmt = getConnection().createStatement();
			String sql = "DELETE from COMPANY where ID=2;";
			stmt.executeUpdate(sql);
			getConnection().commit();

			selectFromAnyTable("COMPANY", true);

			stmt.close();
			if (DataBaseUtil.isDebug_on())
				System.out.println("Delete done successfully");
		} catch (Exception e) {
			if (e instanceof SQLException)
				System.err.println(((SQLException) e).getErrorCode() + ": " + e.getMessage());

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
	}

	/**
	 * Liest eine beliebige Tabelle und gibt diese aus
	 * 
	 * @param tablename
	 *            beliebiger Tabellenname
	 */
	public static void selectFromAnyTable(String tablename) {
		System.out.println("selectFromAnyTable(String tablename)");
		selectFromAnyTable(tablename, true);
	}

	/**
	 * Liest eine beliebige Tabelle und gibt diese aus
	 * 
	 * @param tablename
	 *            beliebiger Tabellenname
	 * @param newlineforcolumn
	 *            erzeugt einen Zeilenumbruch nach jeder Spalte/Column
	 */
	public static void selectFromAnyTable(String tablename, boolean newlineforcolumn) {
		System.out.println("selectFromAnyTable(String tablename, boolean newlineforcolumn)");
		try {
			getConnection().setAutoCommit(false);

			stmt = getConnection().createStatement();
			System.out.println();
			System.out.println("Tabelle : " + tablename);
			System.out.println();
			ResultSet rs = stmt.executeQuery("SELECT * FROM TABLENAME;".replace("TABLENAME", tablename));

			ResultSetMetaData rsmd = rs.getMetaData();

			while (rs.next()) {
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					System.out.print(String.format("%s = %s %s", rsmd.getColumnName(i), rs.getString(i),
							(newlineforcolumn ? "\n" : ((i < rsmd.getColumnCount()) ? ", " : ""))));
				}
				System.out.println();
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			if (e instanceof SQLException)
				System.err.println(((SQLException) e).getErrorCode() + ": " + e.getMessage());
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
	}

	/**
	 * Liest einen Wert aus einem RessourceBundle
	 * 
	 * @param key
	 * @return String Wert aus einem RessourceBundle
	 */
	public static String getString(String key) {
		String result = "-N.A.-";
		try {
			result = bundle.getString(key);
		} catch (Exception ex) {
			System.out.println(ex.getLocalizedMessage());
		}

		return result;
	}

	/**
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws SQLException
	 */
	private static void readNexecuteUpdate(String filename) throws FileNotFoundException, IOException, SQLException {
		sql = readSQLfromFile(filename);
		if (sql.length() > 0) {
			if (isDebug_on()) {
				System.out.println(sql);
			}
			System.out.println(getConnection().createStatement().executeUpdate(sql));
		}
	}

	/**
	 * 
	 * @param filename
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static String readSQLfromFile(String filename) throws FileNotFoundException, IOException {
		StringBuffer sb = new StringBuffer();
		FileReader in = new FileReader(filename);
		BufferedReader br = new BufferedReader(in);

		String line;
		while ((line = br.readLine()) != null) {
			if (isDebug_on())
				System.out.println(line + System.lineSeparator());

			sb.append(line + System.lineSeparator());
		}
		br.close();
		in.close();
		return sb.toString();
	}

	public static void main(String[] args) {

		System.out.println("DataBaseUtil Sample");

		// createTable();
		// System.out.println();
		// insertIntoTable();
		// System.out.println();
		// selectFromAnyTable("COMPANY", false);
		// System.out.println();
		// updateTable();
		// System.out.println();
		// deleteFromTable();

		// SQLITE_MASTER in dieser Tabelle werden alle Tabellen, Indizes und Views usw.
		// gespeichert

		// initDB();
		
		try {
			DataBaseUtil.getConnection().createStatement().executeUpdate("restore from winedivine.db");
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		selectFromAnyTable("SQLITE_MASTER", false);
		selectFromAnyTable("KUNDE", false);
		selectFromAnyTable("BESTELLUNG", false);

	}

	public static boolean isDebug_on() {
		return debug_on;
	}

	public static void setDebug_on(boolean debug_on) {
		DataBaseUtil.debug_on = debug_on;
	}

	static final String dbCreateSql = "" + "BEGIN TRANSACTION;" + "CREATE TABLE KUNDE ("
			+ "	KundenNr INTEGER PRIMARY KEY NOT NULL," + "	Anschrift INTEGER," + "	Rabatt REAL NOT NULL DEFAULT 0,"
			+ "	PicName INTEGER NOT NULL DEFAULT 'FACE25.jpg'"
			+ "	, Bonitaet INTEGER DEFAULT -1, Vertrieb INTEGER DEFAULT -1, Status INTEGER DEFAULT -1);"
			+ "CREATE TABLE BESTELLUNG (" + "	KundenNr INTEGER NOT NULL," + "	Datum TEXT(10) NOT NULL,"
			+ "	WeinSorte TEXT(60) NOT NULL ," + "	Anzahl INTEGER  DEFAULT 0," + "	NettoPreis REAL  DEFAULT 0,"
			+ "	GewRabatt REAL  DEFAULT 0," + "	GesamtPreis REAL  DEFAULT 0" + "	);"
			+ "CREATE UNIQUE INDEX KUNDEN_KundenNr_IDX ON KUNDE (KundenNr);"
			+ "CREATE UNIQUE INDEX BESTELLUNG_KundenNr_IDX ON BESTELLUNG (KundenNr,Datum,WeinSorte);" + "COMMIT;"
			+ "ALTER TABLE KUNDE ADD Name TEXT(60) ;" + "ALTER TABLE KUNDE ADD Strasse TEXT(60) ;"
			+ "ALTER TABLE KUNDE ADD PLZ TEXT(10) ;" + "ALTER TABLE KUNDE ADD Ort TEXT(60) ;"
	;

}
