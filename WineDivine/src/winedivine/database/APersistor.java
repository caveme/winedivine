package winedivine.database;

import java.util.Collection;

public abstract class APersistor<T, A extends Collection<T> , K> {
	/**
	 * Liest alle Datensätze vom Typ T und liefert diese als Collection vom Typ A zurück
	 * 
	 * @return A 
	 */
	public abstract A read();
	/**
	 * Liest alle Datensätze vom Typ T die den Schlüssel Key enthalten und liefert diese 
	 * als Collection vom Typ A zurück
	 * 
	 * @param Key
	 * @return A 
	 */
	public abstract A readAll(K Key);
	/**
	 * Liest ein Object vom Typ T zu dem übergebenen Schlüssel Key
	 * 
	 * @param Key
	 * @return T
	 */
	public abstract T read(K Key);
	/**
	 * Sichert ein Object vom Typ T mit dem Schlüssel Key
	 * 
	 * @param anyObject
	 * @param Key
	 * @return true, wenn das Speichern erfolgreich war
	 */
	public abstract boolean save(T anyObject, K Key);
	/**
	 * Sichert das Object anyObject  vom Typ T mit dem Schlüssel Key
	 * 
	 * @param objectList
	 * @param Key
	 * @return true, wenn das Speichern erfolgreich war
	 */
	public abstract boolean save( A objectList, K Key) ;
	/**
	 * Sicher eine Collection vom Typ A mit Objekten vom Typ T
	 * 
	 * @param objectList
	 * @return true, wenn das Speichern erfolgreich war
	 */
	public abstract boolean save( A objectList) ;
	/**
	 * Löscht alle Objekte zu dem Schlüssel Key
	 * 
	 * @param Key
	 * @return true, wenn das Speichern erfolgreich war
	 */
	public abstract boolean delete(K Key);
	/**
	 * Führt das DELETE Statement auf der Datenbank aus. 
	 * 
	 * @param Key
	 * @return true, wenn das Speichern erfolgreich war
	 * @throws Exception vom Typ E
	 */
	public abstract <E extends Exception> boolean performDelete(K Key) throws E;
	/**
	 * Führt das INSERT Statement auf der Datenbank aus. 
	 * 
	 * @param anyObject
	 * @param Key
	 * @return true, wenn das Speichern erfolgreich war
	 * @throws Exception vom Typ E
	 */
	public abstract <E extends Exception> boolean performInsert(T anyObject, K Key) throws E;
	/**
	 * Führt das UPDATE Statement auf der Datenbank aus.
	 * 
	 * @param anyObject
	 * @param Key
	 * @return true, wenn das Speichern erfolgreich war
	 * @throws Exception vom Typ E
	 */
	public abstract <E extends Exception> boolean performUpdate(T anyObject, K Key) throws E;

}
