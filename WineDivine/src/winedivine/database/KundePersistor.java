package winedivine.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import winedivine.kunde.AKunde;
import winedivine.kunde.Adresse;
import winedivine.kunde.Endverbraucher;
import winedivine.kunde.GesellschaftMLiz;
import winedivine.kunde.Grossverbraucher;
import winedivine.model.Bestellung;
import winedivine.model.Weinsorte;

/**
 * 
 * @author Markus Schubert
 *
 */
public class KundePersistor extends APersistor<AKunde, Collection<AKunde>, Integer> {

	private static boolean result = false;

	private static PreparedStatement pstmt;
	private static ResultSet rs;
	private static ResultSetMetaData rsmd;
	private static String sql;

	private final static String KUNDE_NULL_ERROR = "Kunde ist NULL - speichern nicht möglich!";
	private final static String KUNDE_NOT_FOUND_ERROR = "Kunde KNR konnte nicht gefunden werden!";
	private final static String KUNDE_DELETE_ERROR = "Kunde KNR konnte nicht gelöscht werden!";
	private final static String KUNDE_TYPE_ERROR = "Kunde KNR - fehlerhafter Kundentyp!";
	private final static String KUNDE_SAVE_SUCCESS = "Kunde KNR erfolgreich gespeichert!";
	private final static String KUNDE_UPDATE_SUCCESS = "Kunde KNR erfolgreich aktualisiert!";
	private final static String KUNDE_DELETE_SUCCESS = "Kunde KNR erfolgreich gelöscht!";

	/*
	 * (non-Javadoc)
	 * @see winedivine.database.APersistor#save(java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean save(AKunde kunde, Integer Key) {
		try {
			if (kunde == null)
				throw new RuntimeException(KUNDE_NULL_ERROR);
			else
				result = performInsert(kunde, kunde.getKundNr());
			if (DataBaseUtil.isDebug_on())
				System.out.println(KUNDE_SAVE_SUCCESS.replace("KNR", kunde.getKundNr().toString()));
		} catch (SQLException insertException) {
			try {
				if (insertException.getErrorCode() == 19) {
					result = performUpdate(kunde, kunde.getKundNr());
					if (DataBaseUtil.isDebug_on())
						System.out.println(KUNDE_UPDATE_SUCCESS.replace("KNR", kunde.getKundNr().toString()));
				} else {
					System.out.println(insertException.getErrorCode() + " : " + insertException.getLocalizedMessage());
					insertException.printStackTrace();
				}
			} catch (SQLException updateException) {
				System.out.println(updateException.getErrorCode() + " : " + updateException.getLocalizedMessage());
				updateException.printStackTrace();
			}
		}
		return result;
	}
	/*
	 * (non-Javadoc)
	 * @see winedivine.database.APersistor#save(java.util.Collection)
	 */
	@Override
	public boolean save(Collection<AKunde> objectList) {
		result = true;
		if (objectList != null) {

			for (AKunde kunde : objectList) {
				if (kunde != null)
					result = result && save(kunde, kunde.getKundNr());
			}

		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see winedivine.database.APersistor#read()
	 */
	@Override
	public ArrayList<AKunde> read() {
		ArrayList<AKunde> kunden = new ArrayList<AKunde>();

		try {
			sql = "SELECT * FROM KUNDE ;";
			if (DataBaseUtil.isDebug_on())
				System.out.println(sql);

			pstmt = DataBaseUtil.getConnection().prepareStatement(sql);
			rs = pstmt.executeQuery();
			rsmd = rs.getMetaData();

			while (rs.next()) {
				AKunde kunde = null;

				boolean bonitaet = rs.getInt("Bonitaet") == 1;

				int vertrieb = rs.getInt("Vertrieb");
				int status = rs.getInt("Status");

				if (bonitaet) {
					Endverbraucher ev = new Endverbraucher();
					kunde = ev;
					ev.setBonitaet(bonitaet);
				} else if (vertrieb > -1) {
					GesellschaftMLiz ges = new GesellschaftMLiz();
					kunde = ges;
					ges.setVertrieb(vertrieb);
				} else if (status > -1) {
					Grossverbraucher gro = new Grossverbraucher();
					kunde = gro;
					gro.setStatus(status);
				}

				kunde.setKundNr(rs.getInt("KundenNr"));
				kunde.setRabatt(rs.getDouble("Rabatt"));
				kunde.setPicName(rs.getString("PicName"));

				kunde.setAnschrift(new Adresse(rs.getString("Name"), rs.getString("Strasse"), rs.getString("PLZ"),
						rs.getString("Ort")));

				kunden.add(kunde);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return kunden;
	}
	/*
	 * (non-Javadoc)
	 * @see winedivine.database.APersistor#read(java.lang.Object)
	 */
	@Override
	public AKunde read(Integer knr) {
		AKunde kunde = null;
		try {
			if (knr != null) {

				sql = "SELECT * FROM KUNDE WHERE KUNDENNR = ? ;";
				if (DataBaseUtil.isDebug_on())
					System.out.println(sql);

				pstmt = DataBaseUtil.getConnection().prepareStatement(sql);
				pstmt.setInt(1, knr);
				rs = pstmt.executeQuery();
				rsmd = rs.getMetaData();

				while (rs.next()) {
					boolean bonitaet = rs.getBoolean("Bonitaet");
					int vertrieb = rs.getInt("Vertrieb");
					int status = rs.getInt("Status");

					if (bonitaet) {
						Endverbraucher ev = new Endverbraucher();
						kunde = ev;
						ev.setBonitaet(bonitaet);
					} else if (vertrieb > -1) {
						GesellschaftMLiz ges = new GesellschaftMLiz();
						kunde = ges;
						ges.setVertrieb(vertrieb);
					} else if (status > -1) {
						Grossverbraucher gro = new Grossverbraucher();
						kunde = gro;
						gro.setStatus(status);
					} else {
						throw new RuntimeException(KUNDE_TYPE_ERROR.replace("KNR", knr.toString()));
					}

					kunde.setKundNr(rs.getInt("KundenNr"));
					kunde.setRabatt(rs.getDouble("Rabatt"));
					kunde.setPicName(rs.getString("PicName"));

					kunde.setAnschrift(new Adresse(rs.getString("Name"), rs.getString("Strasse"), rs.getString("PLZ"),
							rs.getString("Ort")));

				}

				if (kunde == null)
					System.out.println(KUNDE_NOT_FOUND_ERROR.replace("KNR", knr.toString()));
				else if (DataBaseUtil.isDebug_on())
					System.out.println(kunde);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return kunde;
	}
	/*
	 * (non-Javadoc)
	 * @see winedivine.database.APersistor#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Integer knr) {
		try {
			if (knr != null) {
				result = performDelete(knr);
				if (DataBaseUtil.isDebug_on())
					System.out.println(KUNDE_DELETE_SUCCESS.replace("KNR", knr.toString()));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(KUNDE_DELETE_ERROR.replace("KNR", knr.toString()));
		}
		return result;
	}
	/*
	 * (non-Javadoc)
	 * @see winedivine.database.APersistor#performUpdate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean performUpdate(AKunde kunde, Integer Key) throws SQLException {
		sql = "UPDATE KUNDE "
				+ "SET Anschrift = ?, Rabatt = ?, PicName = ?, Bonitaet = ?, Vertrieb = ?, Status = ?, Name = ?, Strasse = ?, PLZ = ?, Ort =? "
				+ "WHERE KundenNr = ?";
		if (DataBaseUtil.isDebug_on())
			System.out.println(sql);

		pstmt = DataBaseUtil.getConnection().prepareStatement(sql);

		pstmt.setInt(1, -1);
		pstmt.setDouble(2, kunde.getRabatt());
		pstmt.setString(3, kunde.getPicName());
		if (kunde instanceof Endverbraucher) {
			pstmt.setBoolean(4, ((Endverbraucher) kunde).isBonitaet());
		} else {
			pstmt.setInt(4, -1);
		}
		if (kunde instanceof GesellschaftMLiz) {
			pstmt.setInt(5, ((GesellschaftMLiz) kunde).getVertrieb());
		} else {
			pstmt.setInt(5, -1);
		}
		if (kunde instanceof Grossverbraucher) {
			pstmt.setInt(6, ((Grossverbraucher) kunde).getStatus());
		} else {
			pstmt.setInt(6, -1);
		}
		pstmt.setString(7, kunde.getAnschrift().getName());
		pstmt.setString(8, kunde.getAnschrift().getStrasse_hnr());
		pstmt.setString(9, kunde.getAnschrift().getPlz());
		pstmt.setString(10, kunde.getAnschrift().getOrt());
		pstmt.setInt(11, kunde.getKundNr());

		return pstmt.executeUpdate() > 0;
	}
	/*
	 * (non-Javadoc)
	 * @see winedivine.database.APersistor#performInsert(java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean performInsert(AKunde kunde, Integer key) throws SQLException {
		sql = "INSERT INTO KUNDE ( KundenNr, Anschrift, Rabatt, PicName, Bonitaet, Vertrieb, Status, Name, Strasse, PLZ, Ort ) "
				+ "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ;";
		if (DataBaseUtil.isDebug_on())
			System.out.println(sql);

		pstmt = DataBaseUtil.getConnection().prepareStatement(sql);
		pstmt.setInt(1, kunde.getKundNr());
		pstmt.setInt(2, -1);
		pstmt.setDouble(3, kunde.getRabatt());
		pstmt.setString(4, kunde.getPicName());

		if (kunde instanceof Endverbraucher) {
			pstmt.setBoolean(5, ((Endverbraucher) kunde).isBonitaet());
		} else {
			pstmt.setInt(5, -1);
		}
		if (kunde instanceof GesellschaftMLiz) {
			pstmt.setInt(6, ((GesellschaftMLiz) kunde).getVertrieb());
		} else {
			pstmt.setInt(6, -1);
		}
		if (kunde instanceof Grossverbraucher) {
			pstmt.setInt(7, ((Grossverbraucher) kunde).getStatus());
		} else {
			pstmt.setInt(7, -1);
		}
		pstmt.setString(8, kunde.getAnschrift().getName());
		pstmt.setString(9, kunde.getAnschrift().getStrasse_hnr());
		pstmt.setString(10, kunde.getAnschrift().getPlz());
		pstmt.setString(11, kunde.getAnschrift().getOrt());

		return pstmt.executeUpdate() > 0;
	}
	/*
	 * (non-Javadoc)
	 * @see winedivine.database.APersistor#performDelete(java.lang.Object)
	 */
	@Override
	public boolean performDelete(Integer knr) throws SQLException {

		sql = "DELETE FROM KUNDE ;";
		if (knr != null) {
			sql = sql.replace("KUNDE", " KUNDE WHERE KundenNr = ? ");
		}
		if (DataBaseUtil.isDebug_on())
			System.out.println(sql);

		pstmt = DataBaseUtil.getConnection().prepareStatement(sql);
		if (knr != null) {
			pstmt.setInt(1, knr);
		}

		return pstmt.executeUpdate() > 0;
	}
	/*
	 * (non-Javadoc)
	 * @see winedivine.database.APersistor#readAll(java.lang.Object)
	 */
	@Override
	public Collection<AKunde> readAll(Integer Key) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see winedivine.database.APersistor#save(java.util.Collection, java.lang.Object)
	 */
	@Override
	public boolean save(Collection<AKunde> objectList, Integer Key) {
		return false;
	}

}
