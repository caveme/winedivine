package winedivine.model;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;
/**
 * 
 * @author Markus Schubert
 *
 */
public class Weinsorte implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2051278285610084502L;
	//Name der Weinsorte
	private String weinsorte;
	//Preis je Flasche
	private double preis;
	
	/**
	 * Konstruktor (<em>Name der Weinsort, Preis je Flasche</em>)
	 * 
	 * @param weinsorte
	 * @param preis
	 */
	public Weinsorte(String weinsorte, double preis) {
		this.weinsorte = weinsorte;
		this.preis = preis;
	}
	
	/**
	 * Liefert die Weinsorte als String
	 */
	@Override
	public String toString() {
		
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.GERMAN);
		// TODO Auto-generated method stub
		return String.format(" %-20s a %10s ", getWeinsorte(), nf.format(getPreis()));
	}
	
	/**
	 * Liefert den <em>Namen der Weinsorte</em> 
	 * 
	 * @return weinsorte
	 */
	public String getWeinsorte() {
		return weinsorte;
	}
	/**
	 * Setzen des Namens der Weinsorte
	 * 
	 * @param weinsorte
	 */
	public void setWeinsorte(String weinsorte) {
		this.weinsorte = weinsorte;
	}
	/**
	 * Liefert den <em>Preis je Flasche</em>
	 * 
	 * @return preis
	 */
	public double getPreis() {
		return preis;
	}
	/**
	 * 
	 * @param preis
	 */
	public void setPreis(double preis) {
		this.preis = preis;
	}
	
	/**
	 * 
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(preis);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((weinsorte == null) ? 0 : weinsorte.hashCode());
		return result;
	}
	/**
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Weinsorte other = (Weinsorte) obj;
		if (Double.doubleToLongBits(preis) != Double.doubleToLongBits(other.preis))
			return false;
		if (weinsorte == null) {
			if (other.weinsorte != null)
				return false;
		} else if (!weinsorte.equals(other.weinsorte))
			return false;
		return true;
	}
	
	
}
