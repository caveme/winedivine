package winedivine.model;

import java.util.Date;
import java.io.Serializable;

/**
 * Bestellung Model Klasse
 * 
 * @author mschubert2
 *
 */
public class Bestellung implements Comparable<Bestellung>, Serializable {

	private int anzahl;
	private final Date datum;
	private double gesamtPreis;
	private double gewaehrterRabatt = 0.0;
	private double gesamtPreisNetto;
	private Weinsorte wein;

	/**
	 * 
	 * @param anzahl
	 * @param wein
	 */
	public Bestellung(int anzahl, Weinsorte wein) {
		this(anzahl, wein, new Date());
	}

	public Bestellung(int anzahl, Weinsorte wein, Date datum) {
		this.anzahl = anzahl;
		this.wein = wein;
		this.datum = datum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anzahl;
		result = prime * result + ((datum == null) ? 0 : datum.hashCode());
		long temp;
		temp = Double.doubleToLongBits(gesamtPreis);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(gesamtPreisNetto);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(gewaehrterRabatt);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((wein == null) ? 0 : wein.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bestellung other = (Bestellung) obj;
		if (anzahl != other.anzahl)
			return false;
		if (datum == null) {
			if (other.datum != null)
				return false;
		} else if (!datum.equals(other.datum))
			return false;
		if (Double.doubleToLongBits(gesamtPreis) != Double.doubleToLongBits(other.gesamtPreis))
			return false;
		if (Double.doubleToLongBits(gesamtPreisNetto) != Double.doubleToLongBits(other.gesamtPreisNetto))
			return false;
		if (Double.doubleToLongBits(gewaehrterRabatt) != Double.doubleToLongBits(other.gewaehrterRabatt))
			return false;
		if (wein == null) {
			if (other.wein != null)
				return false;
		} else if (!wein.equals(other.wein))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Bestellung [anzahl=" + anzahl + ", datum=" + datum + ", gesamtPreis=" + gesamtPreis
				+ ", gewaehrterRabatt=" + gewaehrterRabatt + ", gesamtPreisNetto=" + gesamtPreisNetto + ", wein=" + wein
				+ "]";
	}

	@Override
	public int compareTo(Bestellung o) {
		long thisDate = this.datum.getTime();
		long thatDate = o.datum.getTime();
		if (thisDate > thatDate) {
			return -1;
		}
		if (thisDate == thatDate) {
			return 0;
		} else
			return 1;
	}

	/**
	 * 
	 * @return
	 */
	public int getAnzahl() {
		return anzahl;
	}

	/**
	 * 
	 * @param anzahl
	 */
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	/**
	 * 
	 * @return
	 */
	public Date getDatum() {
		return datum;
	}

	/**
	 * 
	 * 
	 * 
	 * /**
	 * 
	 * @return
	 */
	public double getGesamtPreis() {

		return gesamtPreis;
	}

	/**
	 * 
	 * @param gesamtPreis
	 */
	public void setGesamtPreis(double gesamtPreis) {
		this.gesamtPreis = gesamtPreis;
	}

	/**
	 * 
	 * @return
	 */
	public double getGewaehrterRabatt() {
		return gewaehrterRabatt;
	}

	/**
	 * 
	 * @param gewaehrterRabatt
	 */
	public void setGewaehrterRabatt(double gewaehrterRabatt) {
		this.gewaehrterRabatt = gewaehrterRabatt;
	}

	/**
	 * 
	 * @return
	 */
	public double getGesamtPreisNetto() {
		return gesamtPreisNetto;
	}

	/**
	 * 
	 * @param gesamtPreisNetto
	 */
	public void setGesamtPreisNetto(double gesamtPreisNetto) {
		this.gesamtPreisNetto = gesamtPreisNetto;
	}

	public Weinsorte getWein() {
		return wein;
	}

	public void setWein(Weinsorte wein) {
		this.wein = wein;
	}

}
