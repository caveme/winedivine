package winedivine.model;

import java.io.Serializable;
import java.util.Date;
import java.util.TreeMap;

import winedivine.kunde.AKunde;
import winedivine.model.weinsorten.Chardonnay;

public class Bestellungsverwaltung implements Serializable {

	private static final long serialVersionUID = -3892012681766319194L;
	AKunde meinKunde = null;
	TreeMap<Date, Bestellung> bestellungsListe = new TreeMap<Date, Bestellung>();

	public Bestellungsverwaltung(AKunde kunde) {
		this.meinKunde = kunde;
		
		//makeFakeBestellungen(); - nur zum Testen verwenden

	}

	public Bestellung neuBestellung(Weinsorte wein, AVerpackungseinheit verpackung, int stueck) {

		int flaschen = verpackung.getAnzahlFlaschen() * stueck;
		Bestellung neueBestellung = new Bestellung(flaschen, wein);
		neueBestellung.setGewaehrterRabatt(meinKunde.getRabatt());
		neueBestellung.setGesamtPreis(wein.getPreis() * flaschen * neueBestellung.getGewaehrterRabatt());
		neueBestellung.setGesamtPreisNetto(neueBestellung.getGesamtPreis() / 1.19);
		this.setBestllung(neueBestellung);
		return neueBestellung;
	}

	public void setBestllung(Bestellung bestellung) {
		// if(pruefeBestellung(bestellung))
		bestellungsListe.put(bestellung.getDatum(), bestellung);
	}

	public boolean pruefeBestellung(Bestellung bestellung) {
		Bestellung b = bestellungsListe.get(bestellung.getDatum());
		if (bestellung.equals(b)) {
			return true;
		}
		return false;
	}

	public TreeMap<Date, Bestellung> getBestellungsListe() {
		return bestellungsListe;
	}

	public Weinsorte getDummyWeinsorte() {
		Weinsorte wein = new Chardonnay();
		return wein;
	}

	public AVerpackungseinheit getDummyVerpackung() {
		AVerpackungseinheit pack = new Karton();
		return pack;
	}
// Methode zum Erzeugen von Bestellungen zum Testen
	
//	private void makeFakeBestellungen() {
//		
//		Palette palette = new Palette();
//		Weinsorte wein1 = new Chardonnay();
//		
//		Bestellung b1 = neuBestellung(wein1, palette, 2);
//		
//		setBestllung(b1);
//		
//
//	}

}
