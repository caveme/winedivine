package winedivine.model;

public class Karton extends AVerpackungseinheit {

	public Karton() {
		super(12);
	}
	
	public Karton(int anzahlFlaschen) {
		super(anzahlFlaschen);
	}

}
