package winedivine.model;

public abstract class AVerpackungseinheit {
	int anzahlFlaschen = 1;

	public AVerpackungseinheit(int anzahlFlaschen) {
		this.anzahlFlaschen = anzahlFlaschen;
	}
	
	public int getAnzahlFlaschen() {
		return anzahlFlaschen;
	}

	public void setAnzahlFlaschen(int anzahlFlaschen) {
		this.anzahlFlaschen = anzahlFlaschen;
	}

	@Override
	public String toString() {
		return "Art der Verpackung: " + this.getClass().getSimpleName() 
				+ "Anzahl von Flaschen: " + this.getAnzahlFlaschen() + " Flaschen";
	}
}
