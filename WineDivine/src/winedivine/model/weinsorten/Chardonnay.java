package winedivine.model.weinsorten;

import winedivine.model.Weinsorte;

public class Chardonnay extends Weinsorte {

	public Chardonnay() {
		super("Chardonnay", 17.99);
		
	}
}
