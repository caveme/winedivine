package winedivine.model.weinsorten;

import winedivine.model.Weinsorte;

public class Riesling extends Weinsorte {
	public Riesling() {

		super("Riesling", 8.99);
	}
}
