package winedivine.model.weinsorten;

import winedivine.model.Weinsorte;

public class Burgunder extends Weinsorte{

	public Burgunder(){
		super("Grauburgunder", 27.89);
	}
}
