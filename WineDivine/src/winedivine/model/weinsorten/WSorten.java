package winedivine.model.weinsorten;

public enum WSorten {
	CHARDONNAY("Chardonnay", 17.88), BURGUNDER("Grauburgunder", 7.99), RIESLING("Riesling", 27.89);
		
		private String weinname;
		private double preis;
		
		private WSorten(String weinname, double preis) {
			this.weinname = weinname;
			this.preis = preis;
	}
	
	public String getWName(){
		return weinname;
	}
	
	public double getWPreis(){
		return preis;
	}
}
/**
public class EnumTest {

WSorten sorte;

public static void main(String[] args) {
	EnumTest tenum = new EnumTest();
//	WSorten wsorten = new WSorten();
	tenum.sorte = WSorten.CHARDONNAY;
	System.out.println(tenum.sorte.getWName() + " " +  tenum.sorte.getWPreis());
	tenum.sorte = WSorten.BURGUNDER;
	System.out.println(tenum.sorte);
}
}
**/
