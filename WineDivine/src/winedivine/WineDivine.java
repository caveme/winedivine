package winedivine;

import java.util.Locale;

import winedivine.controller.Controller;
import winedivine.ui.WineDivineFrame;
/**
 * WineDivine Starter
 * 
 * @author mschubert2
 *
 */
public class WineDivine {
	/**
	 * main Methode zum starten der Anwendung
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		Controller ctrl = new Controller();
		
		if (args != null && args.length >0){
			if (args[0].equals("en")){
				Locale.setDefault(Locale.ENGLISH);
			}else if (args[0].equals("it")){
				Locale.setDefault(Locale.ITALIAN);
			}else if (args[0].equals("db")){
				ctrl.setUsedb(true);	
			}
		}
		ctrl.setMainframe( new WineDivineFrame(ctrl) );
	}

}
