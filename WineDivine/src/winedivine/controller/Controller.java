package winedivine.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;

import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import winedivine.database.BestellungPersistor;
import winedivine.database.DataBaseUtil;
import winedivine.database.KundePersistor;
import winedivine.kunde.AKunde;
import winedivine.kunde.Endverbraucher;
import winedivine.kunde.GesellschaftMLiz;
import winedivine.kunde.Grossverbraucher;
import winedivine.model.Bestellung;
import winedivine.ui.AboutDialog;
import winedivine.ui.BestellDialog;
import winedivine.ui.IConstants;
import winedivine.ui.KundenDialog;
import winedivine.ui.WineDivineFrame;

/**
 * WineDivine Controller
 * 
 * @author Markus Schubert
 *
 */
public class Controller implements IConstants, ActionListener, ListSelectionListener, MouseListener, WindowListener {

	private WineDivineFrame mainframe;
	private AKunde kunde = null;
	private BestellDialog bestellDialog;
	private KundenDialog kundendialog;

	private boolean usedb;

	/**
	 * 
	 */
	public Controller() {
		this.usedb = false;
	}

	public Controller(boolean usedb) {
		setUsedb(usedb);
	}

	/**
	 * handelt die ActionEvents die der ActionListener empfängt werden hier
	 * behandelt
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// Menü -> sichern
		if (e.getActionCommand().equals(IConstants.AC_beenden)) {
			programExits();
		} else
		//
		if (e.getActionCommand().equals(IConstants.AC_sichern)) {

			if (usedb)
				saveToDataBase();
			else
				saveDataToFile();

		} else
		// Menü -> oeffnen
		if (e.getActionCommand().equals(IConstants.AC_oeffnen)) {

			if (usedb)
				readFromDataBase();
			else
				readDataFromFile();

		} else
		// Ok Button des Bestelldialog
		if (e.getActionCommand().equals(AC_bestellung_neu)) {

			handleNewOrder();

		} else
		// Menü Hilfe->Info
		if (e.getActionCommand().equals(AC_info)) {

			showAboutDialog();

		} else
		// neuer Kunde wurde erstellt und wird der Liste hinzugefügt
		if (e.getActionCommand().equals(AC_kunde_angelegt)) {

			addClientToList();

		} else
		// Menü Bestellung->Erfassen öffnet den Bestelldialog
		if (e.getActionCommand().equals(AC_bestellung)) {

			if (!getMainframe().getPnlKundenListe().getListeMitKunden().isEmpty())
			newOrder();
			else
				JOptionPane.showMessageDialog(getMainframe(), "Bitte zuerst einen Kunden anlegen und diesen auswählen!", getMainframe().getTitle(), JOptionPane.WARNING_MESSAGE);

		} else
		// Kunde neu
		if (e.getActionCommand().equals(AC_einzelV) || e.getActionCommand().equals(AC_gesellschaft)
				|| e.getActionCommand().equals(AC_grossV) || e.getActionCommand().equals(AC_neu)) {

			newClient(e);

		} else
		// Kunde ändern
		if (e.getActionCommand().equals(AC_aendern)) {

			editClient();

		} else
		// Kunde löschen
		if (e.getActionCommand().equals(AC_loeschen)) {

			if (usedb)
				deleteKundeFromDB();

			deleteClient();
		}

	}

	private void deleteKundeFromDB() {
		kunde = (AKunde) getMainframe().getPnlKundenListe().getSelectedItem();
		if (kunde != null) {
			new KundePersistor().delete(kunde.getKundNr());
			new BestellungPersistor().delete(kunde.getKundNr());
		}
	}

	/**
	 * 
	 */
	private void readFromDataBase() {
		//Importiert die Datei winedivine.db in die Memory Datenbank  
		try {
			DataBaseUtil.getConnection().createStatement().executeUpdate("restore from winedivine.db");
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		ArrayList<AKunde> kunden = new KundePersistor().read();

		for (AKunde kunde : kunden) {
			ArrayList<Bestellung> bestellungen = new BestellungPersistor().readAll(kunde.getKundNr());

			for (Bestellung bestellung : bestellungen) {

				kunde.getBestellungsverwaltung().setBestllung(bestellung);

			}

		}
		if (kunden != null) {
			getMainframe().getPnlKundenListe().setListeMitKunden(kunden);
		}

	}

	/**
	 * 
	 */
	private void saveToDataBase() {
		ArrayList<AKunde> kunden = getMainframe().getPnlKundenListe().getListeMitKunden();
		if (kunden != null) {
			for (AKunde kunde : kunden) {
				new KundePersistor().save(kunde, kunde.getKundNr());
				TreeMap<Date, Bestellung> bestellungen = kunde.getBestellungsverwaltung().getBestellungsListe();
				for (Date datum : bestellungen.keySet()) {
					new BestellungPersistor().save(bestellungen.get(datum), kunde.getKundNr());
				}
			}
		}
		//Exportiert die Memory Datenbank in die Datei winedivine.db
		try {
			DataBaseUtil.getConnection().createStatement().executeUpdate("backup to winedivine.db");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Eine neue Bestellung dem Kunden hinzufügen und Views aktualisieren
	 */
	private void handleNewOrder() {
		if (bestellDialog != null) {
			// ermittelt den in der Kundenliste selektierten Kunden
			kunde = (AKunde) getMainframe().getPnlKundenListe().getSelectedItem();

			if (kunde != null) {

				// der Bestellungsverwaltung eine neue Bestellung mit den im Bestelldialog
				// erfassten Daten hinzufügen

				kunde.getBestellungsverwaltung().setBestllung(

						kunde.getBestellungsverwaltung().neuBestellung(bestellDialog.getBestellung().getWein(),
								bestellDialog.getUnit(), bestellDialog.getBestellung().getAnzahl()));

				getMainframe().getPnlBestellung().updateTable(kunde);

				bestellDialog.dispose();
			}
		}
	}

	/**
	 * Daten aus Serialisierung lesen
	 */
	private void readDataFromFile() {
		getMainframe().getPnlKundenListe().setListeMitKunden(
				(ArrayList<AKunde>) Serialize.loadFromFile(Serialize.FileNames.KUNDENLISTE.dateiName));
	}

	/**
	 * Daten via Serialisierung speichern
	 */
	private void saveDataToFile() {
		Serialize.saveToFile(Serialize.FileNames.KUNDENLISTE.dateiName,
				getMainframe().getPnlKundenListe().getListeMitKunden());
	}

	/**
	 * Handling About Dialog
	 */
	private void showAboutDialog() {
		AboutDialog adlg = new AboutDialog(getMainframe());
	}

	/**
	 * Handling Kunde zur Kundenliste hinzufügen
	 */
	private void addClientToList() {
		if (kunde != null) {
			// kunde = kundendialog.getFilledKunde();
			getMainframe().getPnlKundenListe().addItem(kunde);
		}
	}

	/**
	 * Handling Bestelldialog anzeigen
	 */
	private void newOrder() {
		if (bestellDialog == null)
			bestellDialog = new BestellDialog(this.getMainframe(), this);
		else
			bestellDialog.setVisible(true);
	}

	/**
	 * Handling Kunde anlegen
	 */
	public void newClient(ActionEvent e) {
		getMainframe().getPnlKundenTyp().getEinzelV().setSelected(e.getActionCommand().equals(AC_einzelV));
		getMainframe().getPnlKundenTyp().getGesellschaft().setSelected(e.getActionCommand().equals(AC_grossV));
		getMainframe().getPnlKundenTyp().getGrossV().setSelected(e.getActionCommand().equals(AC_gesellschaft));

		if (getMainframe().getPnlKundenTyp().getEinzelV().isSelected()) {
			kunde = new Endverbraucher();
		} else if (getMainframe().getPnlKundenTyp().getGrossV().isSelected()) {
			kunde = new Grossverbraucher();
		} else if (getMainframe().getPnlKundenTyp().getGesellschaft().isSelected()) {
			kunde = new GesellschaftMLiz();
		} else {
			JOptionPane.showMessageDialog(null, "Bitte einen Kundentyp auswaehlen");
		}
		openClientDialog();
	}

	/**
	 * Handling Kunde löschen
	 */
	public void deleteClient() {
		kunde = (AKunde) getMainframe().getPnlKundenListe().getSelectedItem();
		if (kunde == null) {
			JOptionPane.showMessageDialog(null, "Bitte einen Kunden auswaehlen");
		} else if (JOptionPane.showConfirmDialog(this.getMainframe(), "Kunden wirklich loeschen?",
				this.getMainframe().getTitle(), JOptionPane.WARNING_MESSAGE) == 0) {
			getMainframe().getPnlKundenListe().removeItem(getMainframe().getPnlKundenListe().getItemIndex());
			kunde = null;
		}
	}

	/**
	 * Handling Kunde ändern
	 */
	public void editClient() {
		kunde = (AKunde) getMainframe().getPnlKundenListe().getSelectedItem();

		if (kunde == null)
			JOptionPane.showMessageDialog(null, "Bitte einen Kunden auswaehlen");
		else
			openClientDialog();
	}

	/**
	 * Handling Kundendialog
	 */
	private void openClientDialog() {
		// wenn ein Kunde (neu oder ändern) selektiert wurde Kundendialog anzeigen.
		if (kunde != null) {
			kundendialog = new KundenDialog(kunde);
			kundendialog.getKundeAngeButt().addActionListener(this);
			kundendialog.setVisible(true);
		}
	}

	/**
	 * reagiert auf eine Veränderung der Auswahl in der Kundenliste und zeigt
	 * entsprechend
	 */
	@Override
	public void valueChanged(ListSelectionEvent e) {

		// Änderung in der Listauswahl
		if (e.getSource().equals(getMainframe().getPnlKundenListe().getjList())) {
			// wenn Auswahl beendet...
			if (!e.getValueIsAdjusting()) {

				getMainframe().getPnlKundenListe()
						.setItemIndex(getMainframe().getPnlKundenListe().getjList().getSelectedIndex());

				// selektierten Kunden ermitteln und sofern Kunde != null Bestellungen updaten
				kunde = (AKunde) getMainframe().getPnlKundenListe().getjList().getSelectedValue();

				try {
					if (kunde != null) {
						getMainframe().getPnlBestellung().updateTable(kunde);

						getMainframe().getPnlKundenTyp().getEinzelV().setSelected(kunde instanceof Endverbraucher);
						getMainframe().getPnlKundenTyp().getGesellschaft()
								.setSelected(kunde instanceof GesellschaftMLiz);
						getMainframe().getPnlKundenTyp().getGrossV().setSelected(kunde instanceof Grossverbraucher);

						getMainframe().setImage(kunde.getPicName());
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		}
	}

	/**
	 * 
	 */
	public void programExits() {
		// Serialize.saveToFile(Serialize.FileNames.KUNDENLISTE.dateiName,
		// pnlKundenListe.getListeMitKunden());
		System.exit(0);
	}

	/**
	 * Liefert den WineDivineFrame
	 * 
	 * @return WineDivineFrame
	 */
	public WineDivineFrame getMainframe() {
		return mainframe;
	}

	/**
	 * Setzen des WineDivineFrame
	 * 
	 * @param mainframe
	 */
	public void setMainframe(WineDivineFrame mainframe) {
		this.mainframe = mainframe;
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		programExits();
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		if (e.getSource().equals(getMainframe().getPnlKundenListe().getjList())) {
			if (e.getClickCount() == 2) {
				editClient();
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	public boolean isUsedb() {
		return usedb;
	}

	public void setUsedb(boolean usedb) {
		this.usedb = usedb;
		if (usedb)
			DataBaseUtil.validateDB();

	}

}
