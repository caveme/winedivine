package winedivine.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;

public class Serialize {

	public enum FileNames {
		KUNDENLISTE("kundenliste.ser"), BESTELLLISTE("bestellliste.ser");
		public String dateiName;

		FileNames(String str) {
			dateiName = str;
		}
	}

	public static Collection<?> loadFromFile(String fileName) {

		// constructs stream
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream(fileName));
			Collection<?> c = (Collection<?>) in.readObject();
			return c;

		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		} catch (ClassNotFoundException ex) {
			System.out.println(ex.getMessage());
		} finally {
			try {
				in.close();
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
		}
		return null;
	}

	// serialize
	public static void saveToFile(String fileName, Collection<?> c) {

		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream(fileName));
			out.writeObject(c);
			out.flush();
		} catch (IOException ex) {
		} finally {
			try {
				out.close();
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
		}
	}

	// checks if file exists
	public static boolean fileExists(String fileName) {
		File file;
		try {
			file = new File(fileName);
			return file.exists();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
